<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="tm" uri="paytags" %>

<html>
  <head><title>
    <link rel="stylesheet" href="css/style.css" type="text/css" />
  </title></head>

  <div id="header">
  <body>

  <fmt:setLocale value="${sessionScope.currentLocale}" />
  <fmt:setBundle basename="property.loc" />

  <fmt:message key="loc.header.message.select" var="language" />
  <fmt:message key="loc.header.button.en" var="ENG" />
  <fmt:message key="loc.header.button.ru" var="RUS" />


  <div class="lang-tag">
    ${language}
  </div>
  <div class="lang-eng">
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="localization" />
      <input type="hidden" name="language" value="en" />
      <input type="submit" class="button-locale" name="button" value="${ENG}" />
    </form>
  </div>
  <div class="lang-ru">
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="localization" />
      <input type="hidden" name="language" value="ru" />
      <input type="submit" class="button-locale" name="button" value="${RUS}" />
    </form>
  </div>

  <div class="date-tag"><tm:info-time/></div>







  </body>
  </div>
</html>
