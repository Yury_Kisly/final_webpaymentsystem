<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.pagetitle.index" /></title>
  </head>
  <div id="container">
  <body>
    <%-- header including --%>
    <jsp:include page="WEB-INF/jspf/header.jsp" />

    <fmt:message key="loc.pagetitle.index" var="index" />
    <fmt:message key="loc.index.message.enterLogPass" var="enterLogPass" />
    <fmt:message key="loc.index.message.login" var="Login" />
    <fmt:message key="loc.index.message.pass" var="Password" />
    <fmt:message key="loc.index.button.login" var="startLogin" />


    <h3>
      <b>${enterLogPass}</b>
    </h3>

    <form action="controller" method="post">
      <input type="hidden" name="command" value="login" />
      ${Login}<br/>
      <input type="text" name="login" value="" /><br/>
      ${Password}<br/>
      <input type="password" name="password" value=""/><br/>
      ${errorMessage}<br/><br/>
      <input type="submit" class="button-main" name="button" value="${startLogin}"/><br/>
    </form>


    <%-- footer including --%>
    <jsp:include page="WEB-INF/jspf/footer.jsp" />
  </body>
  </div>
</html>
