<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.main.title" /></title>
  </head>

  <div id="container">

  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.main.message.welcome" var="Welcome" />
    <fmt:message key="loc.main.message.logout" var="Logout" />
    <fmt:message key="loc.main.message.clients" var="Clients" />
    <fmt:message key="loc.main.message.cards" var="Cards" />
    <fmt:message key="loc.main.message.newClient" var="newClient" />
    <fmt:message key="loc.main.message.newCard" var="newCard" />

    <h3>${user}, ${Welcome}</h3>

    <table>
      <tr>
        <td>
          <form action="/controller" method="post">
            <input type="hidden" name="command" value="clients" />
            <input type="submit" class="button-main" name="button" value="${Clients}" />
          </form>
        </td>
        <td>
          <form action="/controller" method="post">
            <input type="hidden" name="command" value="sys_cards" />
            <input type="submit" class="button-main" name="button" value="${Cards}"/>
          </form>
        </td>
        <td>
          <form action="/controller" method="post">
            <input type="hidden" name="command" value="new_client" />
            <input type="submit" class="button-main" name="button" value="${newClient}"/>
          </form>
        </td>
        <td>
          <form action="/controller" method="post">
            <input type="hidden" name="command" value="new_card" />
            <input type="submit" class="button-main" name="button" value="${newCard}"/>
          </form>
        </td>
        <td>
          <form action="/controller" method="post">
            <input type="hidden" name="command" value="logout" />
            <input type="submit" class="button-main" name="button" value="${Logout}" />
          </form>
        </td>
      </tr>
    </table>

    </br></br></br>



    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
  </div>
</html>
