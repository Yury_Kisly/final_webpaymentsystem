<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="local.clientlist.mes.info" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />

    <h3>${info}</h3>

    <fmt:message key="local.clientlist.mes.info" var="title" />
    <fmt:message key="local.clientlist.mes.info" var="info" />
    <fmt:message key="local.admsupp.msg.name" var="name" />
    <fmt:message key="local.admsupp.msg.surname" var="surname" />
    <fmt:message key="local.admsupp.msg.passport" var="passport" />
    <fmt:message key="local.admsupp.msg.address" var="address" />
    <fmt:message key="local.admsupp.caption" var="caption" />
    <fmt:message key="local.admsupp.card" var="card" />
    <fmt:message key="local.admsupp.valid" var="valid" />
    <fmt:message key="local.admsupp.csc" var="csc" />
    <fmt:message key="local.admsupp.type" var="type" />
    <fmt:message key="local.admsupp.amount" var="amount" />
    <fmt:message key="local.admsupp.currency" var="currency" />
    <fmt:message key="local.admsupp.status" var="status" />
    <fmt:message key="local.admsupp.bind" var="link" />
    <fmt:message key="local.admsupp.doBind" var="doLink" />
    <fmt:message key="local.button.back" var="Back" />


    <table>
      <tr><td>${name}:</td>
        <td>${client.name}</td></tr>
      <tr><td>${surname}:</td>
        <td>${client.surname}</td></tr>
      <tr><td>${passport}:</td>
        <td>${client.passport}</td></tr>
      <tr><td>${address}:</td>
        <td>${client.address} <br/></td></tr>
    </table>

    <table width="60%" border="1" cellpadding="2" cellspacing="0">
      <caption>${caption}</caption>
      <tr>
        <th>${card}</th>
        <th>${valid}</th>
        <th>${csc}</th>
        <th>${type}</th>
        <th>${amount}</th>
        <th>${currency}</th>
        <th>${status}</th>
      </tr>
      <c:forEach var="card" items="${clientCardList}">
        <tr>
          <td align="center">${card.number}</td>
          <td align="center">${card.valid}</td>
          <td align="center">${card.securityCode}</td>
          <td align="center">${card.type}</td>
          <td align="center">${card.summ}</td>
          <td align="center">${card.currency}</td>
          <td align="center">${card.status}</td>
        </tr>
      </c:forEach>
    </table>
    <br/>
    <h3>${link}</h3>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="link_card" />
      <select name="freecardNum" class="freecardNum" size="1" >
        <c:forEach var="card" items="${freeCardSet}">
          <option value="${card}">${card}</option>
        </c:forEach>
      </select>
      <input type="submit" class="button-main" name="button" value="${doLink}">
    </form>

    ${messageBind}

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_admin_support"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />

  </body>
    </div>
</html>
