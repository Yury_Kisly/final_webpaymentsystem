<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="local.clientinfo.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="local.clientinfo.message.name" var="_name" />
    <fmt:message key="local.clientinfo.message.surname" var="_surname" />
    <fmt:message key="local.clientinfo.message.passport" var="_passport" />
    <fmt:message key="local.clientinfo.message.address" var="_address" />
    <fmt:message key="local.clientinfo.about" var="aboutMe" />
    <fmt:message key="local.button.back" var="Back" />

    <h3>${aboutMe}</h3>

    <table>
      <tr><td>${_name}:</td>
        <td>${user.name}</td></tr>
      <tr><td>${_surname}:</td>
        <td>${user.surname}</td></tr>
      <tr><td>${_passport}:</td>
        <td>${user.passport}</td></tr>
      <tr><td>${_address}:</td>
        <td>${user.address} <br/></td></tr>
    </table>

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_client_info"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
