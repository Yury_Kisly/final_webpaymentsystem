<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.newclient.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.newclient.newreg" var="newReg" />
    <fmt:message key="loc.newclient.login" var="login" />
    <fmt:message key="loc.newclient.pass" var="pass" />
    <fmt:message key="loc.newclient.name" var="name" />
    <fmt:message key="loc.newclient.surname" var="surname" />
    <fmt:message key="loc.newclient.passport" var="passport" />
    <fmt:message key="loc.newclient.address" var="address" />
    <fmt:message key="loc.newclient.reg" var="reg" />

    <fmt:message key="local.button.back" var="Back" />



    <h3>${newReg}</h3>

    <form action="controller" method="post">
      <input type="hidden" name="command" value="reg_client" />

      <table>
        <tr><td width="40%">${login}*</td>
          <td><input type="text" name="newLogin" value="" /></td></tr>
        <tr><td>${pass}*</td>
          <td><input type="password" name="newPass" value="" /></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>${name}*</td>
          <td><input type="text" name="name" value="" /></td></tr>
        <tr><td>${surname}*</td>
          <td><input type="text" name="surname" value="" /></td></tr>
        <tr><td>${passport}*</td>
          <td><input type="text" name="passport" value="" /></td></tr>
        <tr><td>${address}*</td>
          <td><input type="text" name="address" value="" /></td></tr>
      </table>

      <br/>
      <input type="submit" class="button-main" name="button" value="${reg}"/>
    </form>

    ${msgNewClient}<br/><br/>

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_new_client"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
