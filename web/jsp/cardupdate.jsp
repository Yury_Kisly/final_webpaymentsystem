<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.updcard.title" /></title>
  </head>

  <div id="container">
  <body>

    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="local.updcard.card" var="card" />
    <fmt:message key="local.updcard.valid" var="valid" />
    <fmt:message key="local.updcard.csc" var="csc" />
    <fmt:message key="local.updcard.type" var="type" />
    <fmt:message key="local.updcard.amount" var="amount" />
    <fmt:message key="local.updcard.currency" var="currency" />
    <fmt:message key="local.updcard.status" var="status" />
    <fmt:message key="local.updcard.update" var="update" />
    <fmt:message key="local.button.back" var="Back" />

    <br>

    <table width="60%" border="1" cellpadding="2" cellspacing="0">
      <tr>
        <th>${card}</th>
        <th>${valid}</th>
        <th>${csc}</th>
        <th>${type}</th>
        <th>${amount}</th>
        <th>${currency}</th>
        <th>${status}</th>
      </tr>

        <tr>
          <td align="center">${updateCard.number}</td>
          <td align="center">${updateCard.valid}</td>
          <td align="center">${updateCard.securityCode}</td>
          <td align="center">${updateCard.type}</td>
          <td align="center">${updateCard.summ}</td>
          <td align="center">${updateCard.currency}</td>
          <td align="center">${updateCard.status}</td>
        </tr>
    </table>
    <br/>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="do_card_update" />

      <table>
        <tr><td>${valid}</td>
          <td><input type="text" name="valid" value="" /></td></tr>
        <tr><td>${csc}</td>
          <td><input type="text" name="csc" value="" /></td></tr>
        <tr><td>${type}</td>
          <td>
            <select name="cardType" size="1">
              <option value="1">MasterCard</option>
              <option value="2">Visa</option>
              <option value="3">Belcard</option>
            </select>
          </td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>${currency}</td>
          <td>
            <select name="currency" size="1">
              <option value="1">USD</option>
              <option value="2">EUR</option>
              <option value="3">BYR</option>
              <option value="4">RUR</option>
            </select>
          </td></tr>
        <tr><td>${amount}</td>
          <td><input type="text" name="amount" value="" /></td></tr>
        <tr><td>${status}</td>
          <td>
            <select name="status" size="1">
              <option value="1">unblocked</option>
              <option value="2">blocked</option>
            </select>
          </td></tr>
      </table>

      <br/>
      <input type="submit" class="button-main" name="button" value="${update}"/>
    </form>

    ${msgUpdateCard}

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_card_update"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
