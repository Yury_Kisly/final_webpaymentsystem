<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.clpayment.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.clpayment.pay" var="dopay" />
    <fmt:message key="loc.clpayment.card" var="Card" />
    <fmt:message key="loc.clpayment.code" var="Code" />
    <fmt:message key="loc.clpayment.amount" var="Amount" />
    <fmt:message key="loc.clpayment.dopay" var="Do_pay" />
    <fmt:message key="local.button.back" var="Back" />

    <h3>${dopay}</h3>

      <form action="/controller" method="post">
        <input type="hidden" name="command" value="do_payment">

        <table>
          <tr><td width="50%">${Card}</td>
            <td>
              <select name="cardNum" size="1">
                <c:forEach var="card" items="${clientCards}">
                  <option value="${card.number}">${card.number}</option>
                </c:forEach>
              </select>
            </td></tr>
          <tr><td width="50%">${Code}*</td>
            <td><input type="text" name="operation" value="" /></td></tr>
          <tr><td width="50%">${Amount}*</td>
            <td><input type="text" name="amount" value="" /></td></tr>
        </table>
        <br/>
        <input type="submit" class="button-main" name="button" value="${Do_pay}">
      </form>

    ${message}

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_do_payment"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>


    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
