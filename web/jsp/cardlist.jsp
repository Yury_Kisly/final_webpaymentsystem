<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.cardlist.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.cardlist.title" var="caption" />
    <fmt:message key="local.cardlist.mes.card" var="Card" />
    <fmt:message key="local.cardlist.mes.valid" var="Valid" />
    <fmt:message key="local.cardlist.mes.csc" var="Csc" />
    <fmt:message key="local.cardlist.mes.type" var="Type" />
    <fmt:message key="local.cardlist.mes.amount" var="Amount" />
    <fmt:message key="local.cardlist.mes.currency" var="Currency" />
    <fmt:message key="local.cardlist.mes.status" var="Status" />
    <fmt:message key="local.cardlist.unblockCard" var="unblockCard" />
    <fmt:message key="local.cardlist.deleteCard" var="deleteCard" />
    <fmt:message key="local.cardlist.updateCard" var="updateCard" />



    <fmt:message key="local.cardlist.mes.cardOperation" var="cardOperation" />
    <fmt:message key="local.cardlist.btn.Unblock" var="Unblock" />
    <fmt:message key="local.cardlist.btn.Delete" var="Delete" />
    <fmt:message key="local.cardlist.btn.Update" var="Update" />
    <fmt:message key="local.button.back" var="Back" />


    <table width="60%" border="1" cellpadding="2" cellspacing="0">
      <caption><h3>${caption}</h3></caption>
      <tr>
        <th>${Card}</th>
        <th>${Valid}</th>
        <th>${Csc}</th>
        <th>${Type}</th>
        <th>${Amount}</th>
        <th>${Currency}</th>
        <th>${Status}</th>
      </tr>
      <c:forEach var="card" items="${cardList}">
        <tr>
          <td align="center">${card.number}</td>
          <td align="center">${card.valid}</td>
          <td align="center">${card.securityCode}</td>
          <td align="center">${card.type}</td>
          <td align="center">${card.summ}</td>
          <td align="center">${card.currency}</td>
          <td align="center">${card.status}</td>
        </tr>
      </c:forEach>
    </table>

    <br/>

    <h3>${cardOperation}</h3>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="unblock" />
      <div class="clCard">${unblockCard}:</div>
      <input type="text" maxlength="16" name="unBlockCard" value="" />
      <input type="submit" class="button-main" name="button" value="${Unblock}" />
    </form>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="delete_card">
      <div class="clCard">${deleteCard}:</div>
      <input type="text" maxlength="16" name="unBlockCard" value="" />
      <input type="submit" class="button-main" name="button" value="${Delete}" />
    </form>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="update_card" />
      <div class="clCard">${updateCard}:</div>
      <input type="text" maxlength="16" name="unBlockCard" value="" />
      <input type="submit" class="button-main" name="button" value="${Update}" />
    </form>

    ${messageUnblock}
    ${messageDelete}
    ${msgUpdCard}

    <br/><br/>
    <form action="controller" method="post">
      <input type="hidden" name="command" value="back_card_list"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
