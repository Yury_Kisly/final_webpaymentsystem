<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.newcard.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.newcard.newCradReg" var="newCardReg" />
    <fmt:message key="loc.newcard.card" var="card" />
    <fmt:message key="loc.newcard.valid" var="valid" />
    <fmt:message key="loc.newcard.csc" var="csc" />
    <fmt:message key="loc.newcard.type" var="type" />
    <fmt:message key="loc.newcard.currency" var="currency" />
    <fmt:message key="loc.newcard.amount" var="amount" />
    <fmt:message key="loc.newcard.regist" var="regist" />
    <fmt:message key="local.button.back" var="Back" />


    <h3>${newCardReg}</h3>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="reg_card" />

      <table>
        <tr><td>${card}*</td>
          <td><input type="text" class="newReg" name="number" value="" /></td></tr>
        <tr><td>${valid}*</td>
          <td><input type="text" class="newReg" name="valid" value="" /></td></tr>
        <tr><td>${csc}*</td>
          <td><input type="text" class="newReg" name="csc" value="" /></td></tr>
        <tr><td>${type}*</td>
          <td>
            <select name="cardType" class="newReg" size="1">
              <option value="1">MasterCard</option>
              <option value="2">Visa</option>
              <option value="3">Belcard</option>
            </select>
          </td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>${currency}*</td>
          <td>
            <select name="currency" class="newReg" size="1">
            <option value="1">USD</option>
            <option value="2">EUR</option>
            <option value="3">BYR</option>
            <option value="4">RUR</option>
          </select>
        </td></tr>
        <tr><td>${amount}*</td>
          <td><input type="text" class="newReg" name="amount" value="" /></td></tr>
      </table>

      <br/>
      <input type="submit" class="button-main" name="button" value="${regist}"/>
    </form>

    ${msgNewCard}<br/><br/>

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_new_card"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
