<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.clientupdate.title" /></title>
  </head>

  <div id="container">
  <body>

    <jsp:include page="/WEB-INF/jspf/header.jsp" />

    <fmt:message key="loc.clientupdate.login" var="login" />
    <fmt:message key="loc.clientupdate.password" var="password" />
    <fmt:message key="loc.clientupdate.name" var="name" />
    <fmt:message key="loc.clientupdate.surname" var="surname" />
    <fmt:message key="loc.clientupdate.passport" var="passport" />
    <fmt:message key="loc.clientupdate.address" var="address" />
    <fmt:message key="loc.clientupdate.update" var="update" />
    <fmt:message key="local.button.back" var="Back" />

    </br>

    <table>
      <tr><td>${name}:</td>
        <td>${client.name}</td></tr>
      <tr><td>${surname}:</td>
        <td>${client.surname}</td></tr>
      <tr><td>${passport}:</td>
        <td>${client.passport}</td></tr>
      <tr><td>${address}:</td>
        <td>${client.address} <br/></td></tr>
    </table>
    <br/><br/>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="do_client_update" />

      <table>
        <tr><td width="40%">${login}</td>
          <td><input type="text" name="newLogin" value="" /></td></tr>
        <tr><td>${password}</td>
          <td><input type="password" name="newPass" value="" /></td></tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td>${name}</td>
          <td><input type="text" name="name" value="" /></td></tr>
        <tr><td>${surname}</td>
          <td><input type="text" name="surname" value="" /></td></tr>
        <tr><td>${passport}</td>
          <td><input type="text" name="passport" value="" /></td></tr>
        <tr><td>${address}</td>
          <td><input type="text" name="address" value="" /></td></tr>
      </table>

      <br/>
      <input type="submit" class="button-main" name="button" value="${update}"/>
    </form>

    ${msgUpdClient}

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_client_update"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
