<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="local.clientcards.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="local.clientcards.caption" var="caption" />
    <fmt:message key="local.clientcards.card" var="card" />
    <fmt:message key="local.clientcards.valid" var="valid" />
    <fmt:message key="local.clientcards.csc" var="csc" />
    <fmt:message key="local.clientcards.type" var="type" />
    <fmt:message key="local.clientcards.amount" var="amount" />
    <fmt:message key="local.clientcards.currency" var="currency" />
    <fmt:message key="local.clientcards.status" var="status" />
    <fmt:message key="local.clientcards.btn" var="Block" />
    <fmt:message key="local.clientcards.cardblock" var="cardblock" />
    <fmt:message key="local.clientcards.card" var="cardnum" />

    <fmt:message key="local.button.back" var="Back" />

    <table width="60%" border="1" cellpadding="2" cellspacing="0">
      <caption><h3>${caption}</h3></caption>
      <tr>
        <th>${card}</th>
        <th>${valid}</th>
        <th>${csc}</th>
        <th>${type}</th>
        <th>${amount}</th>
        <th>${currency}</th>
        <th>${status}</th>
      </tr>
      <c:forEach var="card" items="${clientCards}">
        <tr>
          <td align="center">${card.number}</td>
          <td align="center">${card.valid}</td>
          <td align="center">${card.securityCode}</td>
          <td align="center">${card.type}</td>
          <td align="center">${card.summ}</td>
          <td align="center">${card.currency}</td>
          <td align="center">${card.status}</td>
        </tr>
      </c:forEach>
    </table>

    <c:if test="${visible}" var="result" scope="page">

      <h3>${cardblock}</h3>
      <form action="/controller" method="post">
        <input type="hidden" name="command" value="block">
        ${cardnum}:
        <input type="text" name="blockCard" value="">
        <input type="submit" class="button-main" name="button" value="${Block}">
      </form>

    </c:if>
    ${messageBlock}


    <br/><br/>
    <form action="controller" method="post">
      <input type="hidden" name="command" value="back_client_cards"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
