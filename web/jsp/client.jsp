<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.pagetitle.client" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />

    <fmt:message key="loc.pagetitle.client" var="clientPage" />
    <fmt:message key="loc.main.message.welcome" var="Welcome" />
    <fmt:message key="loc.main.message.logout" var="Logout" />
    <fmt:message key="loc.main.message.logout" var="Logout" />
    <fmt:message key="loc.client.button.inform" var="inform" />
    <fmt:message key="loc.client.button.cards" var="cards" />
    <fmt:message key="loc.client.button.newpay" var="newpay" />
    <fmt:message key="loc.client.button.transfer" var="transfer" />


    <h4>${user.name} ${user.surname}, ${Welcome}</h4>



    <table>
      <tr>
        <td>
          <form action="controller" method="post">
            <input type="hidden" name="command" value="info" />
            <input type="submit" class="button-main" name="button" value="${inform}" />
          </form>
        </td>
        <td>
          <form action="controller" method="post">
            <input type="hidden" name="command" value="all_cards" />
            <input type="submit" class="button-main" name="button" value="${cards}"/>
          </form>
        </td>
        <td>
          <form action="controller" method="post">
            <input type="hidden" name="command"value="start_pay" />
            <input type="submit" class="button-main" name="button" value="${newpay}" />
          </form>
        </td>
        <td>
          <form action="controller" method="post">
            <input type="hidden" name="command" value="start_transfer" />
            <input type="submit" class="button-main" name="button" value="${transfer}"/>
          </form>
        </td>
        <td>
          <form action="controller" method="post">
            <input type="hidden" name="command" value="logout" />
            <input type="submit" class="button-main" name="button" value="${Logout}" />
          </form>
        </td>
      </tr>
    </table>


    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
