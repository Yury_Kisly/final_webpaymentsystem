<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.cltransfer.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.cltransfer.info" var="info" />
    <fmt:message key="loc.cltransfer.trans" var="dotrans" />
    <fmt:message key="loc.cltransfer.card" var="From_card" />
    <fmt:message key="loc.cltransfer.tocard" var="To_card" />
    <fmt:message key="loc.cltransfer.amount" var="Amount" />
    <fmt:message key="loc.cltransfer.dotrans" var="Do_trans" />
    <fmt:message key="local.button.back" var="Back" />


    <h3>${dotrans}</h3>

    <h5>${info}</h5>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="do_transfer">

      <table>
        <tr><td width="50%">${From_card}</td>
          <td>
            <select name="cardNum" size="1">
              <c:forEach var="card" items="${clientCards}">
                <option value="${card.number}">${card.number}</option>
              </c:forEach>
            </select>
          </td></tr>
        <tr><td width="50%">${To_card}*</td>
          <td><input type="text" name="to_card" value="" /></td></tr>
        <tr><td width="50%">${Amount}*</td>
          <td><input type="text" name="amount" value="" /></td></tr>
      </table>
      <br/>
      <input type="submit" class="button-main" name="button" value="${Do_trans}">
    </form>

    ${messageTrans}

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_do_transfer"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>


    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
    </div>
</html>
