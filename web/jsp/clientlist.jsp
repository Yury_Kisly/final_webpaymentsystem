<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<html>
  <head>
    <fmt:setLocale value="${currentLocale}" />
    <fmt:setBundle basename="property.loc" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <title><fmt:message key="loc.clientlist.title" /></title>
  </head>

  <div id="container">
  <body>
    <jsp:include page="/WEB-INF/jspf/header.jsp" />


    <fmt:message key="loc.clientlist.title" var="caption" />
    <fmt:message key="loc.clientlist.message.id" var="Id" />
    <fmt:message key="local.clientlist.message.name" var="Name" />
    <fmt:message key="local.clientlist.message.surname" var="Surname" />
    <fmt:message key="local.clientlist.message.passport" var="Passport" />
    <fmt:message key="local.clientlist.message.address" var="Address" />
    <fmt:message key="local.clientlist.mes.info" var="info" />
    <fmt:message key="local.clientlist.mes.clNum" var="clNum" />
    <fmt:message key="local.clientlist.clientDel" var="clDel" />
    <fmt:message key="local.clientlist.clientUpd" var="clUpd" />
    <fmt:message key="local.clientlist.mes.next" var="next" />
    <fmt:message key="local.clientlist.btn.Delete" var="Delete" />
    <fmt:message key="local.clientlist.btn.Update" var="Update" />
    <fmt:message key="local.button.back" var="Back" />

    <table width="60%" border="1" cellpadding="2" cellspacing="0">
      <caption><h3>${caption}</h3></caption>
      <tr>
        <th>${Id}</th>
        <th>${Name}</th>
        <th>${Surname}</th>
        <th>${Passport}</th>
        <th>${Address}</th>
      </tr>
      <c:forEach var="client" items="${clientList}">
        <tr>
          <td align="center">${client.id}</td>
          <td align="center">${client.name}</td>
          <td align="center">${client.surname}</td>
          <td align="center">${client.passport}</td>
          <td align="center">${client.address}</td>
        </tr>
      </c:forEach>
    </table>

    <h3>${info}</h3>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="support" />
      <div class="clNum">${clNum}</div>
      <input type="text" size="5" maxlength="5" name="clientId" value="" />
      <input type="submit" class="button-main" name="button" value="${next}" />
    </form>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="delete_client" />
      <div class="clNum">${clDel}</div>
      <input type="text" size="5" maxlength="5" name="clientId" value="" />
      <input type="submit" class="button-main" name="button" value="${Delete}" />
    </form>

    <form action="/controller" method="post">
      <input type="hidden" name="command" value="update_client" />
      <div class="clNum">${clUpd}</div>
      <input type="text" size="5" maxlength="5" name="clientId" value="" />
      <input type="submit" class="button-main" name="button" value="${Update}" />
    </form>

    ${messageSupport}
    ${messageClientDelete}
    ${msgUpdClient}

    <br/><br/>
    <form action="/controller" method="post">
      <input type="hidden" name="command" value="back_client_list"/>
      <input type="submit" class="button-main" name="button" value="${Back}" />
    </form>

    <jsp:include page="/WEB-INF/jspf/footer.jsp" />
  </body>
  </div>
</html>
