package test.by.epam.paysys.initpool;

import org.junit.Assert;
import org.junit.Test;
import by.epam.paysys.pool.ConnectionPool;

public class InitConnectionPoolTest {

    private static int ACTUAL_CONNECTION = 20;

   @Test
    public void checkConectionNumber() {

        int expectedConnection = ConnectionPool.getInstance().getPoolSize();


        Assert.assertEquals("Test failed. Connection number is " + expectedConnection,
                                                                        expectedConnection, ACTUAL_CONNECTION);
    }
}
