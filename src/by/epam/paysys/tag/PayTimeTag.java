package by.epam.paysys.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.GregorianCalendar;

@SuppressWarnings("serial")
public class PayTimeTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {
        GregorianCalendar calendar = new GregorianCalendar();
        String time = String.valueOf(calendar.getTime());

        try {
            JspWriter out = pageContext.getOut();
            out.write(time);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
