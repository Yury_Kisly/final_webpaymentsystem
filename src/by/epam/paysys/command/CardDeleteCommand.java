package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class CardDeleteCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_CARD_NUM = "unBlockCard";
    private static final String MESSAGE = "messageDelete";
    private static final String PAGE_CARD_LIST = "path.page.cardlist";
    private static final String INCORRECT_CARD = "message.incorrectCard";
    private static final String CARD_NOT_EXIST = "message.CardNotExist";
    private static final String ATTR_CARDS = "cardList";
    private static final String DEL_CARD_SUCCESS = "message.delCardSuccess";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String cardNumber = request.getParameter(PARAM_CARD_NUM);

        if(!ProjectValidator.checkCardNumber(cardNumber)) {
            return showMessage(request, INCORRECT_CARD);
        }

        //check if card number exist before deleting
        boolean isCardExist;
        try {
            isCardExist = new AdminService().checkCard(cardNumber);
        } catch (ServiceException e) {
            throw new CommandException("Check card by card number service failed.", e);
        }

        if (!isCardExist) {
            return showMessage(request, CARD_NOT_EXIST);
        }

        //delete card from "cardholder", "card", "bankaccount"
        try {
            new AdminService().deleteCard(cardNumber);
        } catch (ServiceException e) {
            throw new CommandException("Delete card by card number service failed.", e);
        }

        //show rest of cards on page

        ArrayList<Card> cardList;
        try {
            cardList = new AdminService().takeCards();
        } catch (ServiceException e) {
            throw new CommandException("Take cards service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_CARDS, cardList);

        return showMessage(request, DEL_CARD_SUCCESS);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CARD_LIST);
        return page;
    }
}
