package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;


public class AdminSupportCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String ATTR_CLIENT_CARDS = "clientCardList";
    private static final String PARAM_CLIENT_ID = "clientId";
    private static final String INCOR_CLIENT_ID = "message.incorClientId";
    private static final String NO_CLIENT = "message.noClient";
    private static final String MESSAGE = "messageSupport";
    private static final String PAGE_CLIENT_LIST = "path.page.clientlist";
    private static final String PAGE_ADMIN_SUP = "path.page.adminsupport";
    private static final String ATTR_CLIENT = "client";
    private static final String ATTR_FREE_CARDS = "freeCardSet";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page;
        String id = request.getParameter(PARAM_CLIENT_ID);

        //check input id, if false -
        if (!ProjectValidator.checkId(id)) {
            request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(INCOR_CLIENT_ID));
            page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_LIST);
            return page;
        }

        Client client;
        try {
            client = new AdminService().takeClientById(id);
        } catch (ServiceException e) {
            throw new CommandException("Take client by id service failed.", e);
        }

        //client there is no client with input id
        if (client == null) {
            return showMessage(request, NO_CLIENT);
        }

        //client info would be shown on jsp page
        request.getSession(false).setAttribute(ATTR_CLIENT, client);

        //real id from table 'user'
        int realID;
        try {
            realID = new AdminService().takeTableUserId(client.getId());
        } catch (ServiceException e) {
            throw new CommandException("Take tableUserID service failed.", e);
        }

        //find all client cards
        ArrayList<Card> list;

        try {
            list = new AdminService().takeCards(realID);
        } catch (ServiceException e) {
            throw new CommandException("Take cards service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_CLIENT_CARDS, list);

        //find not binded card
        HashSet<BigDecimal> freeCardNumbers;
        try {
            freeCardNumbers = new AdminService().takeNoLinkedCardNumber();
        } catch (ServiceException e) {
            throw new CommandException("Take free cards service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_FREE_CARDS, freeCardNumbers);

        page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_ADMIN_SUP);

        return page;
    }


    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_LIST);
        return page;
    }
}
