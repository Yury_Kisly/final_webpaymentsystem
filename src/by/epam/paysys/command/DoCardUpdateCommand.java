package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;

public class DoCardUpdateCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String UPDATE_CARD = "updateCard";
    private static final String PARAM_VALID = "valid";
    private static final String PARAM_CSC = "csc";
    private static final String PARAM_TYPE = "cardType";
    private static final String PARAM_CURR = "currency";
    private static final String PARAM_AMOUNT = "amount";
    private static final String PARAM_STATUS = "status";
    private static final String MESSAGE = "msgUpdateCard";
    private static final String PAGE_CARD_UPDATE = "path.page.cardupdate";

    private static final String INCORRECT_VALID_PERIOD = "message.incorValidPeriod";
    private static final String INCORRECT_CSC = "message.incorCsc";
    private static final String INCORRECT_AMOUNT = "message.incorAmount";
    private static final String SUCSSES_UPD_CARD = "message.successUpdCard";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        Card card = (Card)request.getSession(false).getAttribute(UPDATE_CARD);

        String valid = request.getParameter(PARAM_VALID);
        String csc = request.getParameter(PARAM_CSC);
        String cardType = request.getParameter(PARAM_TYPE);
        String currency = request.getParameter(PARAM_CURR);
        String amount = request.getParameter(PARAM_AMOUNT);
        String status = request.getParameter(PARAM_STATUS);

        if (!ProjectValidator.checkValid(valid)) {
            return showMessage(request, INCORRECT_VALID_PERIOD);
        }

        if (!ProjectValidator.checkCsc(csc)) {
            return showMessage(request, INCORRECT_CSC);
        }

        if (!ProjectValidator.checkAmount(amount)) {
            return showMessage(request, INCORRECT_AMOUNT);
        }

        //take bank account id by card number
        int bankAccId;
        try {
            bankAccId = new AdminService().takeAccIdByNumber(String.valueOf(card.getNumber()));
        } catch (ServiceException e) {
            throw new CommandException("Take bank acc id service failed.", e);
        }


        //update card data
        try {
            new AdminService().updateCardData(valid, csc, cardType, currency, amount, status, bankAccId);
        } catch (ServiceException e) {
            throw new CommandException("Update card data service failed.", e);
        }

        //take update info about card by number
        Card updateCard;
        try {
            updateCard = new AdminService().takeCardByNumber(String.valueOf(card.getNumber()));
        } catch (ServiceException e) {
            throw new CommandException("Take card by number service failed.", e);
        }

        request.getSession(false).setAttribute(UPDATE_CARD, updateCard);

        return showMessage(request, SUCSSES_UPD_CARD);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CARD_UPDATE);
    }
}
