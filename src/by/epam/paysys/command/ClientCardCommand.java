package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class ClientCardCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String ATTR_USER = "user";
    private static final String ATTR_VISIBLE = "visible";
    private static final String ATTR_CL_CARDS = "clientCards";
    private static final String PAGE_CLIENT_CARDS = "path.page.clientcards";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        Client client = (Client)request.getSession(false).getAttribute(ATTR_USER);	//если в сессии уже был залогинен пользователь
        String page = null;

        //real id from table 'user'
        int realID;
        try {
            realID = new ClientService().takeTableUserId(client.getId());
        } catch (ServiceException e) {
            throw new CommandException("Take tableUserID service failed.", e);
        }

        ArrayList<Card> list;

        try {
            list = new ClientService().takeCards(realID);
        } catch (ServiceException e) {
            throw new CommandException("Take cards service failed.", e);
        }

        if (list != null && !list.isEmpty()) {
            request.getSession(false).setAttribute(ATTR_VISIBLE, true);         // blocking card posibility invisible
        } else {
            request.getSession(false).setAttribute(ATTR_VISIBLE, false);
        }

        request.getSession(false).setAttribute(ATTR_CL_CARDS, list);
        page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_CARDS);


        return page;
    }
}
