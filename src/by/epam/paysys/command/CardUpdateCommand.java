package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;


public class CardUpdateCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_NUMBER = "unBlockCard";
    private static final String INCORRECT_INPUT_DATA = "message.incorrectIn";
    private static final String MESSAGE = "msgUpdCard";
    private static final String PAGE_CARDS = "path.page.cardlist";
    private static final String PAGE_UPD_CARD = "path.page.cardupdate";
    private static final String UPDATE_CARD = "updateCard";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String cardNumber = request.getParameter(PARAM_NUMBER);

        //input card number failed
        if (!ProjectValidator.checkCardNumber(cardNumber)) {
            return showMessage(request, INCORRECT_INPUT_DATA) ;
        }

        //check if card has been already reg
        boolean isCardExist;
        try {
            isCardExist = new AdminService().checkCard(cardNumber);
        } catch (ServiceException e) {
            throw new CommandException("Check card by card number service failed.", e);
        }

        if (!isCardExist) {
            return showMessage(request, INCORRECT_INPUT_DATA) ;
        }

        //take update info about card by number
        Card updateCard;
        try {
            updateCard = new AdminService().takeCardByNumber(cardNumber);
        } catch (ServiceException e) {
            throw new CommandException("Take card by number service failed.", e);
        }

        request.getSession(false).setAttribute(UPDATE_CARD, updateCard);

        return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_UPD_CARD);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CARDS);
        return page;
    }
}
