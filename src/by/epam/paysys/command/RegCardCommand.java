package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;

public class RegCardCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_NUMBER = "number";
    private static final String PARAM_VALID = "valid";
    private static final String PARAM_CSC = "csc";
    private static final String PARAM_TYPE = "cardType";
    private static final String PARAM_CURR = "currency";
    private static final String PARAM_AMOUNT = "amount";
    private static final String MESSAGE = "msgNewCard";
    private static final String PAGE_NEW_CLIENT = "path.page.newCard";

    private static final String SUCSSES_REG_CARD = "message.successRegCard";
    private static final String INCORRECT_INPUT_DATA = "message.incorrectIn";
    private static final String INCORRECT_VALID_PERIOD = "message.incorValidPeriod";
    private static final String INCORRECT_CSC = "message.incorCsc";
    private static final String INCORRECT_AMOUNT = "message.incorAmount";
    private static final String CARD_EXIST = "message.incorCardExist";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page;
        String cardNumber = request.getParameter(PARAM_NUMBER);
        String valid = request.getParameter(PARAM_VALID);
        String csc = request.getParameter(PARAM_CSC);
        String cardType = request.getParameter(PARAM_TYPE);
        String currency = request.getParameter(PARAM_CURR);
        String amount = request.getParameter(PARAM_AMOUNT);

        //input card number failed
        if (!ProjectValidator.checkCardNumber(cardNumber)) {
            page = showMessage(request, INCORRECT_INPUT_DATA) ;
            return page;
        }

        if (!ProjectValidator.checkValid(valid)) {
            page = showMessage(request, INCORRECT_VALID_PERIOD) ;
            return page;
        }

        if (!ProjectValidator.checkCsc(csc)) {
            page = showMessage(request, INCORRECT_CSC) ;
            return page;
        }

        if (!ProjectValidator.checkAmount(amount)) {
            page = showMessage(request, INCORRECT_AMOUNT) ;
            return page;
        }

        //check if card has been already reg
        boolean isCardExist;
        try {
            isCardExist = new AdminService().checkCard(cardNumber);
        } catch (ServiceException e) {
            throw new CommandException("Check card by card number service failed.", e);
        }

        if (isCardExist) {
            page = showMessage(request, CARD_EXIST) ;
            return page;
        }

        try {
            new AdminService().regNewCard(cardNumber, valid, csc, cardType, currency, amount);
        } catch (ServiceException e) {
            throw new CommandException("New card registration service failed.", e);
        }

        return showMessage(request, SUCSSES_REG_CARD);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_NEW_CLIENT);
        return page;
    }
}
