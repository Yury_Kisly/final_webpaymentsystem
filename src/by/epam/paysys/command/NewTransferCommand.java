package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class NewTransferCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String ATTR_CLIENT_ID = "clientId";
    private static final String ATTR_CL_CARDS = "clientCards";
    private static final String PAGE_CLIENT_TRANSFER = "path.page.clienttransfer";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        int clientId = (Integer)request.getSession(false).getAttribute(ATTR_CLIENT_ID);

        //real id from table 'user'
        int realID;
        try {
            realID = new AdminService().takeTableUserId(clientId);
        } catch (ServiceException e) {
            throw new CommandException("Take tableUserID service failed.", e);
        }

        ArrayList<Card> list;
        try {
            list = new ClientService().takeCards(realID);
        } catch (ServiceException e) {
            throw new CommandException("Take cards service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_CL_CARDS, list);

        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_TRANSFER);

        return page;
    }
}
