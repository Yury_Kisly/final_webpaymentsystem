package by.epam.paysys.command;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;


public class CommandHelper {
    private static final Logger LOGGER = Logger.getLogger(CommandHelper.class);
    private static final CommandHelper helper = new CommandHelper();
    private HashMap<CommandType, ICommand> commandMap = new HashMap<>();
    private static final String PARAM_COMMAND = "command";

    private CommandHelper() {
        commandMap.put(CommandType.LOCALIZATION, new LocaleCommand());
        commandMap.put(CommandType.LOGIN, new LoginCommand());
        commandMap.put(CommandType.LOGOUT, new LogoutCommand());
        commandMap.put(CommandType.INFO, new ClientInfoCommand());
        commandMap.put(CommandType.ALL_CARDS, new ClientCardCommand());
        commandMap.put(CommandType.CLIENTS, new ClientListCommand());
        commandMap.put(CommandType.SYS_CARDS, new CardListCommand());
        commandMap.put(CommandType.NEW_CLIENT, new NewClientCommand());
        commandMap.put(CommandType.NEW_CARD, new NewCardCommand());
        commandMap.put(CommandType.REG_CLIENT, new RegClientCommand());
        commandMap.put(CommandType.REG_CARD, new RegCardCommand());
        commandMap.put(CommandType.BLOCK, new BlockCardCommand());
        commandMap.put(CommandType.UNBLOCK, new UnblockCardCommand());
        commandMap.put(CommandType.START_PAY, new NewPaymentCommand());
        commandMap.put(CommandType.DO_PAYMENT, new DoPaymentCommand());
        commandMap.put(CommandType.START_TRANSFER, new NewTransferCommand());
        commandMap.put(CommandType.DO_TRANSFER, new DoTransferCommand());
        commandMap.put(CommandType.SUPPORT, new AdminSupportCommand());
        commandMap.put(CommandType.LINK_CARD, new CardLinkingCommand());
        commandMap.put(CommandType.DELETE_CARD, new CardDeleteCommand());
        commandMap.put(CommandType.DELETE_CLIENT, new ClientDeleteCommand());
        commandMap.put(CommandType.UPDATE_CLIENT, new ClientUpdateCommand());
        commandMap.put(CommandType.DO_CLIENT_UPDATEE, new DoClientUpdateCommand());
        commandMap.put(CommandType.UPDATE_CARD, new CardUpdateCommand());
        commandMap.put(CommandType.DO_CARD_UPDATE, new DoCardUpdateCommand());

        commandMap.put(CommandType.BACK_CLIENT_INFO, new BackClientPageCommand());
        commandMap.put(CommandType.BACK_CLIENT_CARDS, new BackClientPageCommand());
        commandMap.put(CommandType.BACK_CLIENT_LIST, new BackAdminPageCommand());
        commandMap.put(CommandType.BACK_CARD_LIST, new BackAdminPageCommand());
        commandMap.put(CommandType.BACK_DO_PAYMENT, new BackClientPageCommand());
        commandMap.put(CommandType.BACK_DO_TRANSFER, new BackClientPageCommand());
        commandMap.put(CommandType.BACK_NEW_CLIENT, new BackAdminPageCommand());
        commandMap.put(CommandType.BACK_NEW_CARD, new BackAdminPageCommand());
        commandMap.put(CommandType.BACK_ADMIN_SUPPORT, new ClientListCommand());
        commandMap.put(CommandType.BACK_CLIENT_UPDATE, new ClientListCommand());
        commandMap.put(CommandType.BACK_CARD_UPDATE, new CardListCommand());
    }

    public static CommandHelper getInstance() {
        return helper;
    }

    //return command object by key, taken from request
    public ICommand selectCommand(HttpServletRequest request) {
        ICommand commandObj;
        String action = request.getParameter(PARAM_COMMAND);

        if (action == null || action.isEmpty()) {
            LOGGER.info("No such command: " + action);
            return new EmptyCommand();
        }

        CommandType key = CommandType.valueOf(action.toUpperCase());
        commandObj = commandMap.get(key);

        return commandObj;
    }
}
