package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

public class CardLinkingCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_CARD_NUM = "freecardNum";
    private static final String MESSAGE = "messageBind";
    private static final String PAGE_ADM_SUPP = "path.page.adminsupport";
    private static final String CARD_EMPTY = "message.noBindingCards";
    private static final String ATTR_CLIENT = "client";
    private static final String ATTR_CLIENT_CARDS = "clientCardList";
    private static final String SUCCSESS_LINKING = "message.successLinked";
    private static final String ATTR_FREE_CARDS = "freeCardSet";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String cardNumber = request.getParameter(PARAM_CARD_NUM);
        System.out.println("CARD NUMBER:" + cardNumber);

        if (!ProjectValidator.checkCardNumber(cardNumber)) {
            return showMessage(request, CARD_EMPTY);
        }

        Client client = (Client)request.getSession(false).getAttribute(ATTR_CLIENT);
        //id from table 'user'
        int realID;
        try {
            realID = new AdminService().takeTableUserId(client.getId());
        } catch (ServiceException e) {
            throw new CommandException("Take tableUserID service failed.", e);
        }

        //adding free card to client by id
        try {
            new AdminService().linkNewCard(cardNumber, realID);
            //after adding card read clients card one more
            ArrayList<Card> clientCardlist = new AdminService().takeCards(realID);
            request.getSession(false).setAttribute(ATTR_CLIENT_CARDS, clientCardlist);            //обновление карточек
            //update not binded card
            HashSet<BigDecimal> freeCardNumbers = new AdminService().takeNoLinkedCardNumber();
            request.getSession(false).setAttribute(ATTR_FREE_CARDS, freeCardNumbers);
        } catch (ServiceException e) {
            throw new CommandException("Link new card operation failed.", e);
        }

        return showMessage(request, SUCCSESS_LINKING);
    }

    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_ADM_SUPP);
        return page;
    }
}
