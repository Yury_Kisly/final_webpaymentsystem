package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class ClientListCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String ATTR_CLIENTS = "clientList";
    private static final String PAGE_CLIENT_LIST = "path.page.clientlist";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        ArrayList<Client> clientList;
        try {
            clientList = new AdminService().takeClients();
        } catch (ServiceException e) {
            throw new CommandException("Take clients service failed.", e);
        }

        request.getSession().setAttribute(ATTR_CLIENTS, clientList);

        PropertyManager manager = new PropertyManager(ProjectConst.CONFIG);
        String page = manager.getProperty(PAGE_CLIENT_LIST);

        return page;
    }
}
