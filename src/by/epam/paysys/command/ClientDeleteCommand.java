package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class ClientDeleteCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_CLIENT_NUM = "clientId";
    private static final String MESSAGE = "messageClientDelete";
    private static final String INCORRECT_ID = "message.incorrectID";
    private static final String PAGE_CLIENT_LIST = "path.page.clientlist";
    private static final String ATTR_CLIENTS = "clientList";
    private static final String DEL_CLIENT_SUCCESS = "message.delClientSuccess";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String clientId = request.getParameter(PARAM_CLIENT_NUM);

        if (!ProjectValidator.checkId(clientId)) {
            return showMessage(request, INCORRECT_ID);
        }

        //check if client exist before deleting
        boolean isClientExist;
        try {
            isClientExist = new AdminService().checkClient(clientId);
        } catch (ServiceException e) {
            throw new CommandException("Check client by id service failed.", e);
        }

        if (!isClientExist) {
            return showMessage(request, INCORRECT_ID);
        }

        //real id from table 'user'
        int realID;
        try {
            realID = new AdminService().takeTableUserId(Integer.parseInt(clientId));
        } catch (ServiceException e) {
            throw new CommandException("Take tableUserID service failed.", e);
        }

        //delete card from "user", "userinfo",
        try {
            new AdminService().deleteClient(realID);
        } catch (ServiceException e) {
            throw new CommandException("Delete client by user.id service failed.", e);
        }

        //show rest of clients on page
        ArrayList<Client> clientList;
        try {
            clientList = new AdminService().takeClients();
        } catch (ServiceException e) {
            throw new CommandException("Take clients service failed.", e);
        }
        request.getSession(false).setAttribute(ATTR_CLIENTS, clientList);

        return showMessage(request, DEL_CLIENT_SUCCESS);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_LIST);
        return page;
    }
}
