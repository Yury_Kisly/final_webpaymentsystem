package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Client;
import by.epam.paysys.entity.UserType;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.LoginService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;


public class DoClientUpdateCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_LOGIN = "newLogin";
    private static final String PARAM_PASS = "newPass";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_SURNAME = "surname";
    private static final String PARAM_PASSPORT = "passport";
    private static final String PARAM_ADDRESS = "address";
    private static final String ATTR_CLIENT = "client";
    private static final String MESSAGE = "msgUpdClient";
    private static final String PAGE_CLIENT_UPDATE = "path.page.clientupdate";

    private static final String FAILED_LOG = "message.failed.login";
    private static final String FAILED_PASS = "message.failed.password";
    private static final String FAILED_NAME = "message.failed.name";
    private static final String FAILED_SURNAME = "message.failed.surname";
    private static final String FAILED_PASSPORT = "message.failed.passport";
    private static final String FAILED_ADDRESS = "message.failed.address";
    private static final String SUCSSES_UPD_CLIENT = "message.successUpdClient";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASS);
        String name = request.getParameter(PARAM_NAME);
        String surname = request.getParameter(PARAM_SURNAME);
        String passport = request.getParameter(PARAM_PASSPORT);
        String address = request.getParameter(PARAM_ADDRESS);

        Client client = (Client)request.getSession(false).getAttribute(ATTR_CLIENT);

        if (!ProjectValidator.checkLogin(login)) {
            return showMessage(request, FAILED_LOG);
        }
        if (!ProjectValidator.checkPassword(password)) {
            return showMessage(request, FAILED_PASS);
        }
        if (!ProjectValidator.checkName(name)) {
            return showMessage(request, FAILED_NAME);
        }
        if (!ProjectValidator.checkSurname(surname)) {
            return showMessage(request, FAILED_SURNAME);
        }
        if (!ProjectValidator.checkPassport(passport)) {
            return showMessage(request, FAILED_PASSPORT);
        }
        if (!ProjectValidator.checkPassport(passport)) {
            return showMessage(request, FAILED_PASSPORT);
        }
        if (!ProjectValidator.checkAddress(address)) {
            return showMessage(request, FAILED_ADDRESS);
        }


        //update client data
        try {
            new AdminService().updateClientData(client.getId(), name, surname, passport, address, login, password);
        } catch (ServiceException e) {
            throw new CommandException("Update client data service failed.", e);
        }

        //take update client info to show on the page
        try {
            client = new AdminService().takeClientById(String.valueOf(client.getId()));
        } catch (ServiceException e) {
            throw new CommandException("Take client by id service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_CLIENT, client);

        return showMessage(request, SUCSSES_UPD_CLIENT);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_UPDATE);
        return page;
    }
}
