package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;


public class DoPaymentCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_CARD = "cardNum";
    private static final String PARAM_AMOUNT = "amount";
    private static final String MESSAGE = "message";
    private static final String BLOCKED = "blocked";
    private static final String INCORRECT_INPUT_DATA = "message.incorrectIn";
    private static final String INCORRECT_CARD = "message.incorrectCard";
    private static final String BLOCKED_CARD = "message.blockedCard";
    private static final String NOT_ENOUGH_MONEY = "message.nomoney";
    private static final String SUCCESSFUL_PAY = "message.successpay";
    private static final String PAGE_CLIENT_PAYMENT = "path.page.clientpayment";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page = null;
        String card = request.getParameter(PARAM_CARD);
        String amount = request.getParameter(PARAM_AMOUNT);

        //check input param
        if (!ProjectValidator.checkAmount(amount) || !ProjectValidator.checkCardNumber(card)) {
            page = showMessage(request, INCORRECT_INPUT_DATA);
            return page;
        }

        //take update info about card by number
        Card clientCard;
        try {
            clientCard = new ClientService().takeCardByNumber(card);
        } catch (ServiceException e) {
            throw new CommandException("Take card by number service failed.", e);
        }

        //if no card
        if (clientCard == null) {
            page = showMessage(request, INCORRECT_CARD);
            return page;
        }

        //if card has been already blocked
        if (BLOCKED.equals(clientCard.getStatus())) {
            page = showMessage(request, BLOCKED_CARD);
            return page;
        }


        BigDecimal inAmount = new BigDecimal(amount);
        //compare if there are enough money on banking card
        if (inAmount.compareTo(clientCard.getSumm()) > 0) {
            page = showMessage(request, NOT_ENOUGH_MONEY);
            return page;
        }


        //update amount on bankaccount

        //calculate what value should be set to db
        BigDecimal newAmount = clientCard.getSumm().subtract(inAmount);

        ClientService service = new ClientService();
        int accountId;

        try {
            accountId = service.takeAccIdByNumber(card);
        } catch (ServiceException e) {
            throw new CommandException("Take bank account service failed.", e);
        }

        try {
            service.updateBankAccaunt(accountId, newAmount);
        } catch (ServiceException e) {
            throw new CommandException("Update after payment service failed.", e);
        }


        return showMessage(request, SUCCESSFUL_PAY);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_PAYMENT);
        return page;
    }
}
