package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.UserType;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.LoginService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;

public class RegClientCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_LOGIN = "newLogin";
    private static final String PARAM_PASS = "newPass";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_SURNAME = "surname";
    private static final String PARAM_PASSPORT = "passport";
    private static final String PARAM_ADDRESS = "address";
    private static final String MESSAGE = "msgNewClient";
    private static final String PAGE_NEW_CLIENT = "path.page.newClient";

    private static final String FAILED_LOG = "message.failed.login";
    private static final String FAILED_PASS = "message.failed.password";
    private static final String FAILED_NAME = "message.failed.name";
    private static final String FAILED_SURNAME = "message.failed.surname";
    private static final String FAILED_PASSPORT = "message.failed.passport";
    private static final String FAILED_ADDRESS = "message.failed.address";
    private static final String EXIST_LOG_PASS = "message.existLogPass";

    private static final String SUCSSES_REG_CLIENT = "message.successRegClient";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASS);
        String name = request.getParameter(PARAM_NAME);
        String surname = request.getParameter(PARAM_SURNAME);
        String passport = request.getParameter(PARAM_PASSPORT);
        String address = request.getParameter(PARAM_ADDRESS);

        if (!ProjectValidator.checkLogin(login)) {
            return showMessage(request, FAILED_LOG);
        }
        if (!ProjectValidator.checkPassword(password)) {
            return showMessage(request, FAILED_PASS);
        }
        if (!ProjectValidator.checkName(name)) {
            return showMessage(request, FAILED_NAME);
        }
        if (!ProjectValidator.checkSurname(surname)) {
            return showMessage(request, FAILED_SURNAME);
        }
        if (!ProjectValidator.checkPassport(passport)) {
            return showMessage(request, FAILED_PASSPORT);
        }
        if (!ProjectValidator.checkAddress(address)) {
            return showMessage(request, FAILED_ADDRESS);
        }

        UserType type;
        try {
            type = new LoginService().checkLogin(login, password);
        } catch (ServiceException e) {
            throw new CommandException("Check login and password service failed.", e);
        }
        //if no such login and password
        if (type != UserType.GUEST ) {
            return showMessage(request, EXIST_LOG_PASS);
        }

        //reg new client
        try {
            new AdminService().regNewClient(name, surname, passport, address, login, password);
        } catch (ServiceException e) {
            throw new CommandException("Register new client service failed.", e);
        }


        return showMessage(request, SUCSSES_REG_CLIENT);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_NEW_CLIENT);
        return page;
    }
}
