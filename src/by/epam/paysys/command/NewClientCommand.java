package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;

import javax.servlet.http.HttpServletRequest;


public class NewClientCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PAGE_NEW_CLIENT = "path.page.newClient";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_NEW_CLIENT);
        return page;
    }
}
