package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

public class DoTransferCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_FROM_CARD = "cardNum";
    private static final String PARAM_TO_CARD = "to_card";
    private static final String PARAM_AMOUNT = "amount";
    private static final String MESSAGE = "messageTrans";
    private static final String INCORRECT_INPUT_DATA = "message.incorrectIn";
    private static final String UNAVAILABLE = "message.unavailable";
    private static final String INCORRECT_CARD = "message.incorrectCard";
    private static final String INCORRECT_CARD_RECIP = "message.incorrectCardRecip";
    private static final String BLOCKED_CARD = "message.blockedCard";
    private static final String RECIP_BLOCKED_CARD = "message.blockedCardRecip";
    private static final String BLOCKED = "blocked";
    private static final String NOT_ENOUGH_MONEY = "message.noMoney";
    private static final String CURRENCY_NOT_EQUAL = "message.noEqualCur";
    private static final String PAGE_CLIENT_TRANSFER = "path.page.clienttransfer";
    private static final String SUCCESSFUL_TRANS = "message.successtrans";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page = null;
        String fromCard = request.getParameter(PARAM_FROM_CARD);
        String toCard = request.getParameter(PARAM_TO_CARD);
        String amount = request.getParameter(PARAM_AMOUNT);

        //check input param - if false -> show message
        if (!ProjectValidator.checkCardNumber(fromCard) || !ProjectValidator.checkCardNumber(toCard)
                || !ProjectValidator.checkAmount(amount)) {

            page = showMessage(request, INCORRECT_INPUT_DATA);
            return page;
        }

        //don't alow to transfer from number to the same number
        if (fromCard.equals(toCard)) {
            return showMessage(request, UNAVAILABLE);
        }

        //take update info about card by number
        Card fromClientCard;
        try {
            fromClientCard = new ClientService().takeCardByNumber(fromCard);
        } catch (ServiceException e) {
            throw new CommandException("Take card by number service failed.", e);
        }

        //if no card
        if (fromClientCard == null) {
            page = showMessage(request, INCORRECT_CARD);
            return page;
        }

        //if card has been already blocked
        if (BLOCKED.equals(fromClientCard.getStatus())) {
            page = showMessage(request, BLOCKED_CARD);
            return page;
        }

        BigDecimal inAmount = new BigDecimal(amount);
        //compare if there are enough money on banking card
        if (inAmount.compareTo(fromClientCard.getSumm()) > 0) {
            page = showMessage(request, NOT_ENOUGH_MONEY);
            return page;
        }

        //steps with recipient card

        Card toClientCard;
        try {
            toClientCard = new ClientService().takeCardByNumber(toCard);
        } catch (ServiceException e) {
            throw new CommandException("Take recipient card by number service failed.", e);
        }

        //if no card
        if (toClientCard == null) {
            page = showMessage(request, INCORRECT_CARD_RECIP);
            return page;
        }

        //if card has been already blocked
        if (BLOCKED.equals(toClientCard.getStatus())) {
            page = showMessage(request, RECIP_BLOCKED_CARD);
            return page;
        }

        //if currency is not equal on both cards
        if (fromClientCard.getCurrency() == null || !fromClientCard.getCurrency().equals(toClientCard.getCurrency())) {
            page = showMessage(request, CURRENCY_NOT_EQUAL);
            return page;
        }


        ClientService service = new ClientService();
        int fromAccountId;
        int toAccountId;

        try {
            fromAccountId = service.takeAccIdByNumber(fromCard);
            toAccountId = service.takeAccIdByNumber(toCard);
        } catch (ServiceException e) {
            throw new CommandException("Take bank account service failed.", e);
        }

        //trasnfer from card to card
        try {
            service.makeTransfer(fromClientCard, toClientCard, fromAccountId, toAccountId, new BigDecimal(amount));
        } catch (ServiceException e) {
            throw new CommandException("Card transfer service failed.", e);
        }


        return showMessage(request, SUCCESSFUL_TRANS);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_TRANSFER);
        return page;
    }
}
