package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;

import javax.servlet.http.HttpServletRequest;


public class LogoutCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";

    @Override
    public String execute(HttpServletRequest request) {

        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        request.getSession().invalidate();
        return page;
    }
}
