package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Client;
import by.epam.paysys.entity.UserType;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.LoginService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;

public class LoginCommand implements ICommand {
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String ATTR_USER = "user";
    private static final String ATTR_CLIENT_ID = "clientId";
    private static final String ADMIN = "admin";
    private static final String PARAM_ERROR_MESSAGE = "errorMessage";
    private static final String LOGIN_ERROR_MESSAGE = "message.logginerror";
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PAGE_MAIN = "path.page.main";
    private static final String PAGE_CLIENT = "path.page.client";

    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        String page;
        UserType userType;
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);

        //chech input login and pasword
        if (!ProjectValidator.checkLogin(login) || !ProjectValidator.checkPassword(password)) {
            request.setAttribute(PARAM_ERROR_MESSAGE,
                    new PropertyManager(ProjectConst.MESSAGE).getProperty(LOGIN_ERROR_MESSAGE));
            page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
            return page;
        }

        // если есть - работаем уже с базой, иначе на index.jsp сообщение о неверной логинации
        LoginService service = new LoginService();

        try {
            userType = service.checkLogin(login, password);
        } catch (ServiceException e) {
            throw new CommandException("Check login service failed." , e);
        }


        switch(userType) {
            case CLIENT:
                Client client;

                try {
                    client = service.takeClient(login, password);
                } catch (ServiceException e) {
                    throw new CommandException("Take client service failed.", e);
                }
                System.out.println("CLIENT:" + client.getId() + client.getName());
                request.getSession(true).setAttribute(ATTR_USER, client);
                request.getSession(true).setAttribute(ATTR_CLIENT_ID, client.getId());
                page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT);
                break;

            case ADMIN:
                request.getSession(true).setAttribute(ATTR_USER, ADMIN);
                page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_MAIN);
                break;

            default:
                request.setAttribute(PARAM_ERROR_MESSAGE,
                        new PropertyManager(ProjectConst.MESSAGE).getProperty(LOGIN_ERROR_MESSAGE));
                page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);

        }

        return page;
    }
}
