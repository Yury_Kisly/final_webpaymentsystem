package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;

public class ClientUpdateCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_CLIENT_NUM = "clientId";
    private static final String MESSAGE = "msgUpdClient";
    private static final String INCORRECT_ID = "message.incorrectID";
    private static final String PAGE_CLIENT_LIST = "path.page.clientlist";
    private static final String ATTR_CLIENT = "client";
    private static final String PAGE_CLIENT_UPDATE = "path.page.clientupdate";


    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String clientId = request.getParameter(PARAM_CLIENT_NUM);

        if (!ProjectValidator.checkId(clientId)) {
            return showMessage(request, INCORRECT_ID);
        }

        //check if client exist
        boolean isClientExist;
        try {
            isClientExist = new AdminService().checkClient(clientId);
        } catch (ServiceException e) {
            throw new CommandException("Check client by id service failed.", e);
        }

        if (!isClientExist) {
            return showMessage(request, INCORRECT_ID);
        }

        //take client info to modify on the next page
        Client client;
        try {
            client = new AdminService().takeClientById(clientId);
        } catch (ServiceException e) {
            throw new CommandException("Take client by id service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_CLIENT, client);

        return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_UPDATE);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_LIST);
        return page;
    }
}
