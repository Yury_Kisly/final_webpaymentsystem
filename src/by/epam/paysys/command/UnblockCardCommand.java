package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class UnblockCardCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String ATTR_ALL_CARDS = "cardList";
    private static final String PARAM_UNBLOCK_CARD = "unBlockCard";
    private static final String MESSAGE = "messageUnblock";
    private static final String NO_CARD = "message.noCard";
    private static final String INCORRECT_CARD = "message.incorrectCard";
    private static final String PAGE_CARD_LIST = "path.page.cardlist";
    private static final String UNBLOCKED_CARD = "message.unblockedCard";
    private static final String SUCCESSFUL_UNBLOCKED = "message.successunblocked";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page = null;
        ArrayList<Card> list = (ArrayList<Card>)request.getSession(false).getAttribute(ATTR_ALL_CARDS);

        String cardNumber = request.getParameter(PARAM_UNBLOCK_CARD);

        if (list == null || list.isEmpty()) {
            page = showMessage(request, NO_CARD);
            return page;
        }

        //incorrect card number
        if (!ProjectValidator.checkCardNumber(cardNumber)) {
            page = showMessage(request, INCORRECT_CARD);
            return page;
        }

        //card has been already unblocked
        if (!ProjectValidator.checkBlockedCard(list, cardNumber)) {
            page = showMessage(request, UNBLOCKED_CARD);
            return page;
        }

        try {
            new AdminService().unblockCard(cardNumber);
            //after updating database read cards one more
            list = new AdminService().takeCards();
            request.getSession(false).setAttribute(ATTR_ALL_CARDS, list);            //обновление карточек
        } catch (ServiceException e) {
            throw new CommandException("Unblocking service failed.", e);
        }
        return showMessage(request, SUCCESSFUL_UNBLOCKED);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CARD_LIST);
        return page;
    }
}
