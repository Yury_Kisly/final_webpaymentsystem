package by.epam.paysys.command;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ClientService;
import by.epam.paysys.service.ServiceException;
import by.epam.paysys.validator.ProjectValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public class BlockCardCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PARAM_BLOCK_CARD = "blockCard";
    private static final String ATTR_CLIENT_ID = "clientId";
    private static final String MESSAGE = "messageBlock";
    private static final String ATTR_CL_CARDS = "clientCards";
    private static final String PAGE_CLIENT_CARDS = "path.page.clientcards";
    private static final String INCORRECT_CARD = "message.incorrectCard";
    private static final String BLOCKED_CARD = "message.blockedCard";
    private static final String NO_CARD = "message.noCard";
    private static final String SUCCESSFUL_BLOCKED = "message.successblocked";
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String page = null;
        ArrayList<Card> list = (ArrayList<Card>)request.getSession(false).getAttribute(ATTR_CL_CARDS);

        String cardNumber = request.getParameter(PARAM_BLOCK_CARD);

        if (list == null || list.isEmpty()) {
            page = showMessage(request, NO_CARD);
            return page;
        }

        //incorrect card number
        if (!ProjectValidator.checkCardNumber(cardNumber)) {
            page = showMessage(request, INCORRECT_CARD);
            return page;
        }

        //card has been already blocked
        if (ProjectValidator.checkBlockedCard(list, cardNumber)) {
            page = showMessage(request, BLOCKED_CARD);
            return page;
        }

        //real id from table 'user'
        int clientId = (Integer)request.getSession(false).getAttribute(ATTR_CLIENT_ID);
        int realID;
        try {
            realID = new AdminService().takeTableUserId(clientId);
        } catch (ServiceException e) {
            throw new CommandException("Take tableUserID service failed.", e);
        }

        try {
            new ClientService().blockCard(cardNumber);
            //after updating database read clients card one more

            list = new ClientService().takeCards(realID);
            request.getSession(false).setAttribute(ATTR_CL_CARDS, list);            //обновление карточек
        } catch (ServiceException e) {
            throw new CommandException("Blocking service failed.", e);
        }


        return showMessage(request, SUCCESSFUL_BLOCKED);
    }

    //set attribute message to show on page
    private String showMessage(HttpServletRequest request, String message) {
        request.setAttribute(MESSAGE, new PropertyManager(ProjectConst.MESSAGE).getProperty(message));
        String page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_CLIENT_CARDS);
        return page;
    }
}
