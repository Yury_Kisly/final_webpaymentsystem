package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;

import javax.servlet.http.HttpServletRequest;


public class EmptyCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";

    public String execute(HttpServletRequest request) {
        //no command or strait using controller
        PropertyManager manager = new PropertyManager(ProjectConst.CONFIG);
        String page = manager.getProperty(PAGE_INDEX);
        return page;
    }
}
