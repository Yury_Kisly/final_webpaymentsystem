package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.entity.Card;
import by.epam.paysys.resource.PropertyManager;
import by.epam.paysys.service.AdminService;
import by.epam.paysys.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


public class CardListCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String ATTR_CARDS = "cardList";
    private static final String PAGE_CARD_LIST = "path.page.cardlist";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        ArrayList<Card> cardList;
        try {
            cardList = new AdminService().takeCards();
        } catch (ServiceException e) {
            throw new CommandException("Take cards service failed.", e);
        }

        request.getSession(false).setAttribute(ATTR_CARDS, cardList);

        PropertyManager manager = new PropertyManager(ProjectConst.CONFIG);
        String page = manager.getProperty(PAGE_CARD_LIST);

        return page;
    }
}
