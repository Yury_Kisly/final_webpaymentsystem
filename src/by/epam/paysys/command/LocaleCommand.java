package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;

import javax.servlet.http.HttpServletRequest;


public class LocaleCommand implements ICommand {
    private static final String PARAM_LANGUAGE = "language";
    private static final String ATTR_LOCALE = "currentLocale";
    private static final String ATTR_LAST_PAGE = "lastPage";
    private static final String PAGE_INDEX = "path.page.index";

    @Override
    public String execute(HttpServletRequest request) {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        String locale = request.getParameter(PARAM_LANGUAGE);       //take value(en, ru) by name(language) in index.jsp
        request.getSession(true).setAttribute(ATTR_LOCALE, locale); //set selected value(en, ru)

        String page;

        //define path to jsp page
        PropertyManager manager = new PropertyManager(ProjectConst.CONFIG);

        //запомнить имя предыдущей страницы для локали
        page = (String)request.getSession(true).getAttribute(ATTR_LAST_PAGE);
        if (page == null) {
            page = manager.getProperty(PAGE_INDEX);
        }


        return page;
    }
}
