package by.epam.paysys.command;

/**
 * Created by Yura on 10.01.2016.
 */
public class CommandException extends Exception {
    public CommandException(String str) { super(str); }
    public CommandException(Exception e) { super(e); }
    public CommandException(String str, Exception e) { super(str, e); }
}
