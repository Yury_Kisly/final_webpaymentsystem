package by.epam.paysys.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Yura on 29.12.2015.
 */
public interface ICommand {
    String execute(HttpServletRequest request) throws CommandException;
}
