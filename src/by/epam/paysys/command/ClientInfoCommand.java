package by.epam.paysys.command;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;

import javax.servlet.http.HttpServletRequest;


public class ClientInfoCommand implements ICommand {
    private static final String PAGE_INDEX = "path.page.index";
    private static final String PAGE_CLIENT_INFO = "path.page.clientinfo";

    @Override
    public String execute(HttpServletRequest request) {

        //if session dead
        if (request.getSession(false) == null) {
            return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
        }

        PropertyManager manager = new PropertyManager(ProjectConst.CONFIG);
        String page = manager.getProperty(PAGE_CLIENT_INFO);
        return page;
    }
}
