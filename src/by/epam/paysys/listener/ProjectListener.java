package by.epam.paysys.listener;

import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.pool.ConnectionPool;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@WebListener()
public class ProjectListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ProjectListener.class);

    public void contextInitialized(ServletContextEvent sce) {
        ConnectionPool pool;
        int attemptNum = 3;

        checkFile("config.properties");
        checkFile("dbMySQL.properties");
        checkFile("loc_en.properties");
        checkFile("loc_ru.properties");
        checkFile("message.properties");

        //attempt to init connection pool again, if exception would be
        do {
            pool = ConnectionPool.getInstance();
            if (pool != null) {
                LOGGER.debug("Connection pool has been created.");
            } else {
                --attemptNum;
            }
        } while (pool == null && attemptNum > 0);

        if (pool == null) {
            LOGGER.fatal("Connection pool instance failed");
            throw new RuntimeException("Connection pool instance failed.");
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        try {
            ConnectionPool.getInstance().clearConnectionQueue();
        } catch (SQLException e) {
            LOGGER.error("Error closing the connection.", e);
        }
    }

    private void checkFile(String fileName) {

        String[] arr = fileName.split("\\.");

        try {
            ResourceBundle.getBundle("property." + arr[0]);
        } catch (MissingResourceException e) {
            LOGGER.fatal("Property file \"" + fileName + "\" doesn't exist!");
            throw new RuntimeException("Property file \"" + fileName + "\" doesn't exist!");
        }
    }

}
