package by.epam.paysys.pool;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * To prevent connection pool from insertion "savage connection". Connection taken from pool cant return to it because of
 * fullness pool.
 */

public class ProxyConnection implements Connection {
    private Connection cn;

    ProxyConnection(Connection connection) {        //only in package
        this.cn = connection;
    }

    //this method must return connection to the pool
    @Override
    public void close() {
        ConnectionPool.getInstance().returnConnection(this);
    }

    void realClose() throws SQLException {
        if (cn.isClosed()) {
            throw new SQLException("Attempt to close closed connection");
        }
        cn.close();
    }

    @Override
    public <T> T unwrap(Class<T> arg0) throws SQLException {
        return cn.unwrap(arg0);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return cn.isWrapperFor(iface);
    }

    @Override
    public void abort(Executor executor) throws SQLException {
        cn.abort(executor);
    }

    @Override
    public void clearWarnings() throws SQLException {
        cn.clearWarnings();
    }

    @Override
    public void commit() throws SQLException {
        cn.commit();
    }

    @Override
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        return cn.createArrayOf(typeName, elements);
    }

    @Override
    public Blob createBlob() throws SQLException {
        return cn.createBlob();
    }

    @Override
    public Clob createClob() throws SQLException {
        return cn.createClob();
    }

    @Override
    public NClob createNClob() throws SQLException {
        return cn.createNClob();
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        return cn.createSQLXML();
    }

    @Override
    public Statement createStatement() throws SQLException {
        return cn.createStatement();
    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
        return cn.createStatement(resultSetType, resultSetConcurrency);
    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
            throws SQLException {
        return cn.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        return cn.createStruct(typeName, attributes);
    }

    @Override
    public boolean getAutoCommit() throws SQLException {
        return cn.getAutoCommit();
    }

    @Override
    public String getCatalog() throws SQLException {
        return cn.getCatalog();
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        return cn.getClientInfo();
    }

    @Override
    public String getClientInfo(String name) throws SQLException {
        return cn.getClientInfo(name);
    }

    @Override
    public int getHoldability() throws SQLException {
        return cn.getHoldability();
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        return cn.getMetaData();
    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        return cn.getNetworkTimeout();
    }

    @Override
    public String getSchema() throws SQLException {
        return cn.getSchema();
    }

    @Override
    public int getTransactionIsolation() throws SQLException {
        return cn.getTransactionIsolation();
    }

    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException {
        return cn.getTypeMap();
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        return cn.getWarnings();
    }

    @Override
    public boolean isClosed() throws SQLException {
        return cn.isClosed();
    }

    @Override
    public boolean isReadOnly() throws SQLException {
        return cn.isReadOnly();
    }

    @Override
    public boolean isValid(int timeout) throws SQLException {
        return cn.isValid(timeout);
    }

    @Override
    public String nativeSQL(String sql) throws SQLException {
        return cn.nativeSQL(sql);
    }

    @Override
    public CallableStatement prepareCall(String sql) throws SQLException {
        return cn.prepareCall(sql);
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
            throws SQLException {
        return cn.prepareCall(sql, resultSetType, resultSetConcurrency);
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
                                         int resultSetHoldability) throws SQLException {
        return cn.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return cn.prepareStatement(sql);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
        return cn.prepareStatement(sql, autoGeneratedKeys);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
        return cn.prepareStatement(sql, columnIndexes);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
        return cn.prepareStatement(sql, columnNames);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
            throws SQLException {
        return cn.prepareStatement(sql, resultSetType, resultSetConcurrency);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
                                              int resultSetHoldability) throws SQLException {
        return cn.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        cn.releaseSavepoint(savepoint);
    }

    @Override
    public void rollback() throws SQLException {
        cn.rollback();
    }

    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
        cn.rollback();
    }

    @Override
    public void setAutoCommit(boolean autoCommit) throws SQLException {
        cn.setAutoCommit(autoCommit);
    }

    @Override
    public void setCatalog(String catalog) throws SQLException {
        cn.setCatalog(catalog);
    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
        cn.setClientInfo(properties);
    }

    @Override
    public void setClientInfo(String name, String value) throws SQLClientInfoException {
        cn.setClientInfo(name, value);
    }

    @Override
    public void setHoldability(int holdability) throws SQLException {
        cn.setHoldability(holdability);
    }

    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
        cn.setNetworkTimeout(executor, milliseconds);
    }

    @Override
    public void setReadOnly(boolean readOnly) throws SQLException {
        cn.setReadOnly(readOnly);

    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
        return cn.setSavepoint();
    }

    @Override
    public Savepoint setSavepoint(String name) throws SQLException {
        return cn.setSavepoint(name);
    }

    @Override
    public void setSchema(String schema) throws SQLException {
        cn.setSchema(schema);
    }

    @Override
    public void setTransactionIsolation(int level) throws SQLException {
        cn.setTransactionIsolation(level);
    }

    @Override
    public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
        cn.setTypeMap(map);
    }
}
