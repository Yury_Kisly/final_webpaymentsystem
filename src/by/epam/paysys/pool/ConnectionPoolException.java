package by.epam.paysys.pool;

/**
 * Created by Yura on 31.12.2015.
 */
public class ConnectionPoolException extends Exception {

    public ConnectionPoolException(String msg) {
        super(msg);
    }
    public ConnectionPoolException(String msg, Exception e) {
        super(msg, e);
    }
    public ConnectionPoolException(Exception e) {
        super(e);
    }
}
