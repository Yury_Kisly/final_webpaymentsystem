package by.epam.paysys.pool;


import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.resource.PropertyManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public final class ConnectionPool {
    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
    private static final String PROPERTY_URL = "db.url";
    private static final String PROPERTY_LOGIN = "db.login";
    private static final String PROPERTY_PASSWORD = "db.password";
    private static final String PROPERTY_POOLSIZE = "db.poolsize";
    private static final String PUT_USER = "user";
    private static final String PUT_PASSWORD = "password";
    private static final String PUT_ENCOD = "characterEncoding";
    private static final String ENCOD = "UTF-8";
    private static final Integer WAITING_TIME = 2000;
    private ArrayBlockingQueue<ProxyConnection> connectionQueue;
    private ArrayBlockingQueue<ProxyConnection> givenConnectionQueue;

    private static ConnectionPool instance;
    private static ReentrantLock lock = new ReentrantLock();
    private static AtomicBoolean refBoolean = new AtomicBoolean(false);
    private static AtomicBoolean isPermitted = new AtomicBoolean(true);
    private final int DEFAULT_SIZE = 10;

    private String url;
    private String login;
    private String password;
    private int poolSize;

    private ConnectionPool() {
        PropertyManager manager = new PropertyManager(ProjectConst.DB_MY_SQL);
        url = manager.getProperty(PROPERTY_URL);
        login = manager.getProperty(PROPERTY_LOGIN);
        password = manager.getProperty(PROPERTY_PASSWORD);

        try {
            poolSize = Integer.valueOf(manager.getProperty(PROPERTY_POOLSIZE));
        } catch (NumberFormatException e) {
            poolSize = DEFAULT_SIZE;
        }

        initPoolData();
    }

    public int getPoolSize() { return connectionQueue.size(); }

    public static ConnectionPool getInstance() {
        if(!refBoolean.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    refBoolean.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    //using by ServletContextListener when app starts
    private void initPoolData() {

        Properties properties = new Properties();
        properties.put(PUT_USER, login);
        properties.put(PUT_PASSWORD, password);
        properties.put(PUT_ENCOD, ENCOD);

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOGGER.fatal("Driver registration failed", e);
            throw new RuntimeException("Driver registration failed", e);
        }

        try {
            connectionQueue = new ArrayBlockingQueue<>(poolSize);
            givenConnectionQueue = new ArrayBlockingQueue<>(poolSize);

            //creating connections, adding to blocking queue
            for (int index = 0; index < poolSize; index++) {
                Connection connection = DriverManager.getConnection(url, properties);
                ProxyConnection proxyConnection = new ProxyConnection(connection);
                connectionQueue.offer(proxyConnection);
            }
        } catch (SQLException e) {
            LOGGER.error("Attempt to create connection pool successful again");
            instance = null;
        }
    }

    public ProxyConnection takeConnection() throws ConnectionPoolException {
        ProxyConnection connection = null;

        //allows to take connection
        if (isPermitted.get()) {

            try {
                connection = connectionQueue.take();
                givenConnectionQueue.add(connection);
            } catch (InterruptedException e) {
                throw new ConnectionPoolException("Can't take connection", e);
            }
            LOGGER.debug("Connection was taken from the pool");
        } else {
            LOGGER.debug("Wasn't permited to take connection from the pool");
        }
        return connection;
    }

    //return Connection to the ConnectionPool instead of closing it
    public void returnConnection(ProxyConnection cn) {
        givenConnectionQueue.remove(cn);
        connectionQueue.add(cn);
        LOGGER.debug("Connection has returned to the pool.");
    }

    //using by ServletContextListener when app close
    // 1.Запрещаем выдавать новые connection
    // 2.Ожидание, чтобы отданные connection вернулись в свободную очередь
    // 3.закрываем given очередь, затем основную
    public void clearConnectionQueue() throws SQLException {
        isPermitted.set(false);

        try {
            TimeUnit.MILLISECONDS.sleep(WAITING_TIME);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }

        //closeConnectionQueue(givenConnectionQueue);
        closeConnectionQueue(connectionQueue);
    }

    private void closeConnectionQueue(ArrayBlockingQueue<ProxyConnection> queue) throws SQLException{
        ProxyConnection connection;
        while((connection = queue.poll()) != null) {
            connection.realClose();                       //real close Connection
        }
    }
}
