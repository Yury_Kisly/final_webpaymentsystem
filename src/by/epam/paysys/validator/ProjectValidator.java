package by.epam.paysys.validator;


import by.epam.paysys.entity.Card;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class ProjectValidator {
    private static final String BLOCK = "blocked";
    private static final String REGEX_LOGIN = "[\\w]{4,15}";
    private static final String REGEX_PASSWORD = ".{4,15}";
    private static final String REGEX_CARD = "\\d{16}";
    private static final String REGEX_AMOUNT = "\\d+";
    private static final String REGEX_NAME_SURNAME = "[\\wА-Яа-я]{2,}";
    private static final String REGEX_PASSPORT = "[A-Z]{2}\\d{7}";
    private static final String REGEX_VALID = "(0[1-9]|1[0-2])/(1[6-9])";
    private static final String REGEX_CSC = "\\d{4,5}";
    private static final String REGEX_ID = "\\d+";



    public static boolean checkLogin(String login) {
        if (login == null || login.isEmpty()) { return false; }
        return Pattern.matches(REGEX_LOGIN, login) ? true : false;
    }

    public static boolean checkPassword(String password) {
        if (password == null || password.isEmpty()) { return false; }
        return Pattern.matches(REGEX_PASSWORD, password) ? true : false;
    }

    public static boolean checkAmount(String amount) {
        if (amount == null || amount.isEmpty()) { return false; }
        return Pattern.matches(REGEX_AMOUNT, amount) ? true : false;
    }

    public static boolean checkCardNumber(String card) {
        if (card == null || card.isEmpty()) { return false; }
        return Pattern.matches(REGEX_CARD, card) ? true : false;

    }

    public static boolean checkBlockedCard(ArrayList<Card> list, String card) {
        boolean isTrue = false;

        BigDecimal number = new BigDecimal(card);
        //if card status is blocked
        for(Card elem : list) {

            if(elem.getNumber().equals(number) && BLOCK.equals(elem.getStatus())) {
                isTrue = true;
            }
        }

        return isTrue;
    }

    public static boolean checkName(String name) {
        if (name == null || name.isEmpty()) { return false; }
        return Pattern.matches(REGEX_NAME_SURNAME, name) ? true : false;
    }

    public static boolean checkSurname(String surname) {
        if (surname == null || surname.isEmpty()) { return false; }
        return Pattern.matches(REGEX_NAME_SURNAME, surname) ? true : false;
    }

    public static boolean checkPassport(String pass) {
        if (pass == null || pass.isEmpty()) { return false; }
        return Pattern.matches(REGEX_PASSPORT, pass) ? true : false;
    }

    public static boolean checkAddress(String address) {
        return address != null || !address.isEmpty() ?  true : false;
    }

    public static boolean checkValid(String validDate) {
        if (validDate == null || validDate.isEmpty()) { return false; }
        return Pattern.matches(REGEX_VALID, validDate) ? true : false;
    }

    public static boolean checkCsc(String csc) {
        if (csc == null || csc.isEmpty()) { return false; }
        return Pattern.matches(REGEX_CSC, csc) ? true : false;
    }

    public static boolean checkId(String id) {
        if (id == null || id.isEmpty()) { return false; }
        return Pattern.matches(REGEX_ID, id) ? true : false;
    }

}
