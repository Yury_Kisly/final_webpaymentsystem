package by.epam.paysys.controller;

import by.epam.paysys.command.CommandException;
import by.epam.paysys.command.CommandHelper;
import by.epam.paysys.command.ICommand;
import by.epam.paysys.constant.ProjectConst;
import by.epam.paysys.exception.ProjectException;
import by.epam.paysys.resource.PropertyManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/controller") //no use <servlet> and <servlet-mapping>
public class Controller extends HttpServlet {
    private static final String ATTR_LAST_PAGE = "lastPage";
    private static final String PAGE_INDEX = "path.page.index";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (CommandException e) {
            throw new ProjectException("Command failed.", e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response)
                                                            throws ServletException, IOException, CommandException {

        String page;                                     //  otnositelnyj path to jsp page
        CommandHelper helper = CommandHelper.getInstance();
        //obtaining command object to execute request
        ICommand command = helper.selectCommand(request);
        //obtaining jsp page address
        page = command.execute(request);
        //for locale memorize last page
        request.getSession().setAttribute(ATTR_LAST_PAGE, page);

        if (page != null) {
            request.getRequestDispatcher(page).forward(request, response);
        } else {
            page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}
