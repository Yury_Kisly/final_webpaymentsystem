package by.epam.paysys.entity;


public class Client {
    private int id;
    private String name;
    private String surname;
    private String passport;
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object ref) {
        if (this == ref) { return true; }
        if (ref == null || getClass() != ref.getClass()) { return false; }

        Client client = (Client)ref;

        if (id != client.getId()) { return false; }
        if (name != null ? !name.equals(client.getName()) : client.getName() != null) { return false; }
        if (surname != null ? !surname.equals(client.getSurname()) : client.getSurname() != null) { return false; }
        if (passport != null ? !passport.equals(client.getPassport()) : client.getPassport() != null) { return false; }
        if (address != null ? !address.equals(client.getAddress()) : client.getAddress() != null) { return false; }
        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (passport != null ? passport.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Client{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", passport='").append(passport).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
