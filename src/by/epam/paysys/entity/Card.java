package by.epam.paysys.entity;

import java.math.BigDecimal;

/**
 * Created by Yura on 08.01.2016.
 */
public class Card {
    private BigDecimal number;
    private String valid;
    private String status;
    private BigDecimal summ;
    private String currency;
    private String type;
    private String securityCode;

    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getSumm() {
        return summ;
    }

    public void setSumm(BigDecimal summ) {
        this.summ = summ;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    @Override
    public boolean equals(Object ref) {
        if (this ==ref) { return true; }
        if (ref == null || this.getClass() != ref.getClass()) { return false; }

        Card card = (Card)ref;

        if (number != null ? !number.equals(card.getNumber()) : card.getNumber() != null) { return false; }
        if (valid != null ? !valid.equals(card.getValid()) : card.getValid() != null) { return false; }
        if (status != null ? !status.equals(card.getStatus()) : card.getStatus() != null) { return false; }
        if (summ != null ? !summ.equals(card.getSumm()) : card.getSumm() != null) { return false; }
        if (currency != null ? !currency.equals(card.getCurrency()) : card.getCurrency() != null) { return false; }
        if (type != null ? !type.equals(card.getType()) : card.getType() != null) { return false; }
        if (securityCode != null ?
                    !securityCode.equals(card.getSecurityCode()) : card.getSecurityCode() != null) { return false; }

        return true;
    }

    @Override
    public int hashCode() {
        int result = getNumber() != null ? number.hashCode() : 0;
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (summ != null ? summ.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (securityCode != null ? securityCode.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Card{");
        sb.append("number=").append(number);
        sb.append(", valid='").append(valid).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", summ=").append(summ);
        sb.append(", currency='").append(currency).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", securityCode='").append(securityCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
