package by.epam.paysys.entity;

/**
 * Created by Yura on 04.01.2016.
 */
public enum UserType {
    CLIENT, ADMIN, GUEST
}
