package by.epam.paysys.dao;


import by.epam.paysys.entity.Card;
import by.epam.paysys.pool.ConnectionPool;
import by.epam.paysys.pool.ConnectionPoolException;
import by.epam.paysys.pool.ProxyConnection;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClientDAO {
    private static final Logger LOGGER = Logger.getLogger(ClientDAO.class);
    private static final ClientDAO DAO = new ClientDAO();
    private static final int ID_BLOCKED = 2;

    private static final String CARD_BLOCKING = "UPDATE card SET status=? WHERE cardNumber=?";
    private static final String UPDATE_ACC = "UPDATE bankaccount SET amount=? WHERE id=?";


    private ClientDAO() {}
    public static ClientDAO getInstance() { return DAO; }



    public void blockCard(String cardNumber) throws DAOException {
        LOGGER.info("Select client blocking card");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CARD_BLOCKING);
            statement.setInt(1, ID_BLOCKED);
            statement.setString(2, cardNumber);

            LOGGER.debug("Prepare Statement finished");

            statement.executeUpdate();

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Select client blocking card failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Blocking card prepare statement failed.", e);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public void updateBankAccaount(int accountId, BigDecimal newAmount) throws DAOException {
        LOGGER.info("Update after payment.");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(UPDATE_ACC);
            statement.setBigDecimal(1, newAmount);
            statement.setInt(2, accountId);

            LOGGER.debug("Prepare Statement finished");

            statement.executeUpdate();

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Update after payment failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Update after payment statement failed.", e);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public void makeTransfer(int fromAccountId, int toAccountId, BigDecimal fromAmount,
                                                                        BigDecimal toAmount) throws DAOException {

        LOGGER.info("Make transfer form: " + fromAccountId + " to: " + toAccountId);
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            try {
                connection = ConnectionPool.getInstance().takeConnection();
                connection.setAutoCommit(false);                    //for transaction

                //from
                statement = connection.prepareStatement(UPDATE_ACC);
                statement.setBigDecimal(1, fromAmount);
                statement.setInt(2, fromAccountId);
                statement.executeUpdate();
                //to
                statement = connection.prepareStatement(UPDATE_ACC);
                statement.setBigDecimal(1, toAmount);
                statement.setInt(2, toAccountId);
                statement.executeUpdate();

                connection.commit();                            //for transaction
                connection.setAutoCommit(true);

            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException("Transfer prepare statement failed.", e);
            }
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Transfer failed.", exp);
        } catch (SQLException exp) {
            throw new DAOException("Transaction: transfer failed", exp);
        } finally {
            close(statement);
            connection.close();
        }

    }


    private void close(PreparedStatement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Failed prepare statement closing");
        }
    }
}
