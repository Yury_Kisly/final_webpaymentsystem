package by.epam.paysys.dao;

import by.epam.paysys.entity.Client;
import by.epam.paysys.entity.UserType;
import by.epam.paysys.pool.ConnectionPool;
import by.epam.paysys.pool.ConnectionPoolException;
import by.epam.paysys.pool.ProxyConnection;
import java.sql.PreparedStatement;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;


public class UserDAO {
    private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
    private static final UserDAO DAO = new UserDAO();

    private static final String CHEAK_LOGIN =
                            "SELECT type FROM role WHERE RoleID = (SELECT role FROM user WHERE login=? and password=?)";

    private static final String CLIENT_INFO =
                    "SELECT userinfo.id, userinfo.name, userinfo.surname, userinfo.passport, userinfo.adress " +
                    "FROM userinfo INNER JOIN user ON userinfo.id=user.info_id " +
                    "WHERE user.login=? AND user.password=?";


    private UserDAO() {}

    public static UserDAO getInstance() { return DAO; }


    public UserType checkUser(String login, String password) throws DAOException {
        LOGGER.info("Check login and password");
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        UserType userType = UserType.GUEST;

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CHEAK_LOGIN);
            statement.setString(1, login);
            statement.setString(2, password);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит проверяем роль
            if (resultSet.next()) {
                String role = resultSet.getString("type");
                switch (UserType.valueOf(role.toUpperCase())) {
                    case CLIENT:
                        userType = UserType.CLIENT;
                        break;

                    default:
                        userType = UserType.ADMIN;

                }
            }
            LOGGER.trace(userType + " defined");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection for user check failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement for user check failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return userType;
    }

    public Client takeClient(String login, String password) throws DAOException {
        LOGGER.info("Take client info");
        Client client = new Client();
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CLIENT_INFO);
            statement.setString(1, login);
            statement.setString(2, password);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                client.setId(resultSet.getInt("id"));
                client.setName(resultSet.getString("name"));
                client.setSurname(resultSet.getString("surname"));
                client.setPassport(resultSet.getString("passport"));
                client.setAddress(resultSet.getString("adress"));
            }
            LOGGER.trace("Client info was set");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Client info connection failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Client info prepare statement failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return client;
    }

    private void close(PreparedStatement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Failed prepare statement closing");   //TODO or DAOException?????
        }
    }
}
