package by.epam.paysys.dao;

public class ProjDAOFactory {

    private ProjDAOFactory() { }

    public static ClientDAO takeClientDAO() {
        return ClientDAO.getInstance();
    }

    public static AdminDAO takeAdminDAO() {
        return AdminDAO.getInstance();
    }

    public static UserDAO takeUserDAO() {
        return UserDAO.getInstance();
    }
    public static CommonDAO takeCommonDAO() {
        return CommonDAO.getInstance();
    }
}
