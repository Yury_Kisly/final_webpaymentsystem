package by.epam.paysys.dao;


public class DAOException extends Exception {
    public DAOException(String str) { super(str); }
    public DAOException(Exception e) { super(e); }
    public DAOException(String str, Exception e) { super(str, e); }

}
