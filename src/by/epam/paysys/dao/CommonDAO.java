package by.epam.paysys.dao;


import by.epam.paysys.entity.Card;
import by.epam.paysys.pool.ConnectionPool;
import by.epam.paysys.pool.ConnectionPoolException;
import by.epam.paysys.pool.ProxyConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CommonDAO {
    private static final Logger LOGGER = Logger.getLogger(CommonDAO.class);
    private static final CommonDAO DAO = new CommonDAO();

    private static final String BANK_ACCOUNT_ID = "SELECT accountID FROM card WHERE cardNumber=?";

    private static final String CARD_BY_NUMBER =
            "SELECT card.cardNumber, card.validDate, card.csc, cardtype.type, cardstatus.status, " +
                    "ss.amount, ss.currencyType " +
                    "FROM card " +
                    "INNER JOIN cardtype ON (card.cardType=cardtype.id) " +
                    "INNER JOIN cardstatus ON (card.status=cardstatus.id) " +
                    "INNER JOIN ( " +

                    "SELECT bankaccount.id, bankaccount.amount, " +
                    "currency.currencyType " +
                    "FROM bankaccount " +
                    "INNER JOIN currency ON (bankaccount.currencyType=currency.id) " +

                    ") ss ON (card.accountID=ss.id) " +
                    "WHERE card.cardNumber =?";

    private static final String CARD_INFO =
            "SELECT card.cardNumber, card.validDate, card.csc, cardtype.type, cardstatus.status, " +
                    "ss.amount, ss.currencyType " +
                    "FROM card " +
                    "INNER JOIN cardtype ON (card.cardType=cardtype.id) " +
                    "INNER JOIN cardstatus ON (card.status=cardstatus.id) " +
                    "INNER JOIN ( " +

                    "SELECT bankaccount.id, bankaccount.amount, " +
                    "currency.currencyType " +
                    "FROM bankaccount " +
                    "INNER JOIN currency ON (bankaccount.currencyType=currency.id) " +

                    ") ss ON (card.accountID=ss.id) " +
                    "WHERE card.cardNumber IN (SELECT cardNumber FROM cardholder WHERE user=?)";

    private static final String REAL_ID =
            "SELECT user.id FROM user INNER JOIN userinfo ON user.info_id=userinfo.id AND userinfo.id=?";



    private CommonDAO() {}
    public static CommonDAO getInstance() { return DAO; }

    public int takeAccIdByNumber(String cardNumber) throws DAOException {
        LOGGER.info("Take bank account id by card number");
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        int accountId = -1;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(BANK_ACCOUNT_ID);
            statement.setString(1, cardNumber);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                accountId = resultSet.getInt("accountID");
            }

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Take bank account id by card number failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Take bank account id by card number prepare statement failed.", e);
        } finally {
            close(statement);
            connection.close();
        }

        return accountId;
    }

    public Card takeCardByNumber(String cardNumber) throws DAOException {
        LOGGER.info("Take card by card number");
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Card card = null;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CARD_BY_NUMBER);
            statement.setString(1, cardNumber);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                card = new Card();
                card.setNumber(resultSet.getBigDecimal("cardNumber"));
                card.setValid(resultSet.getString("validDate"));
                card.setSecurityCode(resultSet.getString("csc"));
                card.setType(resultSet.getString("type"));
                card.setStatus(resultSet.getString("status"));
                card.setSumm(resultSet.getBigDecimal("amount"));
                card.setCurrency(resultSet.getString("currencyType"));
            }

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Select client blocking card failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Blocking card prepare statement failed.", e);
        } finally {
            close(statement);
            connection.close();
        }

        return card;
    }

    public ArrayList<Card> takeCards(int clientId) throws DAOException{
        LOGGER.info("Select all client cards by id:" + clientId);
        ArrayList<Card> cardList = new ArrayList<>();
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CARD_INFO);
            statement.setInt(1, clientId);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Card card = new Card();

                card.setNumber(resultSet.getBigDecimal("cardNumber"));
                card.setValid(resultSet.getString("validDate"));
                card.setSecurityCode(resultSet.getString("csc"));
                card.setType(resultSet.getString("type"));
                card.setStatus(resultSet.getString("status"));
                card.setSumm(resultSet.getBigDecimal("amount"));
                card.setCurrency(resultSet.getString("currencyType"));

                cardList.add(card);
            }
            LOGGER.trace("Card list was formed");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Select all cards connection failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Client cards prepare statement failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return cardList;
    }

    public int takeTableUserId(int id) throws DAOException {
        LOGGER.info("Take real id from table 'user' by info id: " + id);
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        int realID = 0;

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(REAL_ID);
            statement.setInt(1, id);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит клиент найден
            if (resultSet.next()) {
                realID = resultSet.getInt("id");
            }
            LOGGER.trace("Client was founded");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection for client by id failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement for client by id failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return realID;
    }

    private void close(PreparedStatement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Failed prepare statement closing");
        }
    }
}
