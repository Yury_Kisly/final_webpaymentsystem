package by.epam.paysys.dao;

import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;
import by.epam.paysys.pool.ConnectionPool;
import by.epam.paysys.pool.ConnectionPoolException;
import by.epam.paysys.pool.ProxyConnection;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;


public class AdminDAO {
    private static final Logger LOGGER = Logger.getLogger(AdminDAO.class);
    private static final AdminDAO DAO = new AdminDAO();
    private static final int ROLE = 1;
    private static final int ID_UNBLOCKED = 1;

    private static final String CLIENT_LIST =
            "SELECT userinfo.id, userinfo.name, userinfo.surname, " +
            "userinfo.passport, userinfo.adress " +
            "FROM userinfo " +
            "INNER JOIN user ON (user.info_id=userinfo.id) " +
            "WHERE user.role=?";

    private static final String CARD_LIST =
            "SELECT card.cardNumber, card.validDate, card.csc, cardtype.type, cardstatus.status, " +
            "ss.amount, ss.currencyType " +
            "FROM card " +
            "INNER JOIN cardtype ON (card.cardType=cardtype.id) " +
            "INNER JOIN cardstatus ON (card.status=cardstatus.id) " +
            "INNER JOIN ( " +

            "SELECT bankaccount.id, bankaccount.amount, " +
            "currency.currencyType " +
            "FROM bankaccount  " +
            "INNER JOIN currency ON (bankaccount.currencyType=currency.id) " +
            "INNER JOIN bankname ON (bankaccount.bankID=bankname.id) " +

            ") ss ON (card.accountID=ss.id);";

    private static final String CARD_UNBLOCKING = "UPDATE card SET status=? WHERE cardNumber=?";

    private static final String NEW_CLIENT_INFO =
            "INSERT INTO userinfo (name, surname, passport, adress) VALUES (?, ?, ?, ?)";

    private static final String NEW_CLIENT =
            "INSERT INTO user (info_id, login, password) VALUES (last_insert_id(), ?, ?)";

    private static final String CHECK_CARD =
            "SELECT cardNumber FROM card WHERE cardNumber=?";

    private static final String NEW_BANK_ACC =
            "INSERT INTO bankaccount (currencyType, amount) VALUES (?, ?)";

    private static final String NEW_CARD =
            "INSERT INTO card (cardNumber, validDate, csc, cardType, accountID) VALUES (?, ?, ?, ?, last_insert_id())";

    private static final String CLIENT_BY_ID =
            "SELECT id, name, surname, passport, adress FROM userinfo WHERE id = ?";

    private static final String NO_BIND_CARD_LIST =
            "SELECT card.cardNumber FROM card LEFT JOIN cardholder ON card.cardNumber=cardholder.cardNumber " +
                    "WHERE cardholder.cardNumber IS NULL";

    private static final String CARD_LINK = "INSERT INTO cardholder (user, cardNumber) VALUES (?, ?)";

    private static final String DEL_CARD_HOLDER = "DELETE FROM cardholder WHERE cardNumber=?";
    private static final String DEL_CARD =
            "DELETE card, bankaccount FROM card, bankaccount WHERE card.accountID=bankaccount.id AND card.cardNumber=?";


    private static final String CHECK_ID = "SELECT id FROM userinfo WHERE id=?";

    private static final String DELETE_CLIENT =
            "DELETE user, userinfo FROM user, userinfo WHERE userinfo.id=user.info_id AND user.id=?";

    private static final String UPDATE_CLIENT =
            "UPDATE user SET login=?, password=? WHERE info_id=?";

    private static final String UPDATE_CLIENT_INFO =
            "UPDATE userinfo SET name=?, surname=?, passport=?, adress=? WHERE id=?";

    private static final String UPDATE_BANK_ACC =
            "UPDATE bankaccount SET currencyType=?, amount=? WHERE id=?";

    private static final String UPDATE_CARD =
            "UPDATE card SET validDate=?, csc=?, cardType=?, status=? WHERE accountID=?";





    private AdminDAO() {}

    public static AdminDAO getInstance() { return DAO; }


    public ArrayList<Client> takeClients() throws DAOException {
        LOGGER.info("Take client list");
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        ArrayList<Client> list = new ArrayList<>();

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CLIENT_LIST);
            statement.setInt(1, ROLE);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит проверяем роль
            while (resultSet.next()) {
                Client client = new Client();
                client.setId(resultSet.getInt("id"));
                client.setName(resultSet.getString("name"));
                client.setSurname(resultSet.getString("surname"));
                client.setPassport(resultSet.getString("passport"));
                client.setAddress(resultSet.getString("adress"));
                list.add(client);
                LOGGER.trace("Client formed");
            }
            LOGGER.trace("Client list was formed");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection for client list failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement for client list failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return list;
    }

    public ArrayList<Card> takeCards() throws DAOException {
        LOGGER.info("Take card list by admin");
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        ArrayList<Card> list = new ArrayList<>();

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CARD_LIST);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит проверяем роль
            while (resultSet.next()) {
                Card card = new Card();

                card.setNumber(resultSet.getBigDecimal("cardNumber"));
                card.setValid(resultSet.getString("validDate"));
                card.setSecurityCode(resultSet.getString("csc"));
                card.setType(resultSet.getString("type"));
                card.setStatus(resultSet.getString("status"));
                card.setSumm(resultSet.getBigDecimal("amount"));
                card.setCurrency(resultSet.getString("currencyType"));
                list.add(card);
                LOGGER.trace("Card formed");
            }
            LOGGER.trace("Card list was formed");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection for card list failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement for card list failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return list;
    }

    public void unblockCard(String cardNumber) throws DAOException {
        LOGGER.info("Select unblocking card");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CARD_UNBLOCKING);
            statement.setInt(1, ID_UNBLOCKED);
            statement.setString(2, cardNumber);

            LOGGER.debug("Prepare Statement finished");

            statement.executeUpdate();

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Select unblocking card failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Unblocking card prepare statement failed.", e);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public void regNewClient(String name, String surname, String passport, String address,String login,
                             String password) throws DAOException {

        LOGGER.info("Add new client");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            try {
                connection = ConnectionPool.getInstance().takeConnection();
                connection.setAutoCommit(false);                    //for transaction

                statement = connection.prepareStatement(NEW_CLIENT_INFO);
                statement.setString(1, name);
                statement.setString(2, surname);
                statement.setString(3, passport);
                statement.setString(4, address);
                statement.executeUpdate();

                statement = connection.prepareStatement(NEW_CLIENT);
                statement.setString(1, login);
                statement.setString(2, password);
                statement.executeUpdate();

                connection.commit();                            //for transaction
                connection.setAutoCommit(true);

            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException("Add new client info prepare statement failed.", e);
            }
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Add new client info failed.", exp);
        } catch (SQLException exp) {
            throw new DAOException("Transaction add client failed", exp);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public boolean checkCard(String cardNumber) throws DAOException {
        LOGGER.info("Check card by card number: " + cardNumber);
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        boolean isCardExist = false;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CHECK_CARD);
            statement.setString(1, cardNumber);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                isCardExist = true;
            }

        } catch (ConnectionPoolException e) {
            throw new DAOException("Connection to check card by number failed.", e);
        } catch (SQLException exp) {
            throw new DAOException("Prepare statement to check card by number failed.", exp);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return isCardExist;
    }

    public void regNewCard(String cardNumber, String valid, String csc, String cardType,
                           String currency, String amount) throws DAOException {

        LOGGER.info("Add new card");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            try {
                connection = ConnectionPool.getInstance().takeConnection();
                connection.setAutoCommit(false);                    //for transaction

                statement = connection.prepareStatement(NEW_BANK_ACC);
                statement.setString(1, currency);
                statement.setString(2, amount);
                statement.executeUpdate();

                statement = connection.prepareStatement(NEW_CARD);
                statement.setString(1, cardNumber);
                statement.setString(2, valid);
                statement.setString(3, csc);
                statement.setString(4, cardType);
                statement.executeUpdate();

                connection.commit();                            //for transaction
                connection.setAutoCommit(true);

            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException("Add new card prepare statement failed.", e);
            }
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Add new card failed.", exp);
        } catch (SQLException exp) {
            throw new DAOException("Transaction add card failed", exp);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public Client takeClientById(String id) throws DAOException {
        LOGGER.info("Take client by id: " + id);
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        Client client = null;

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CLIENT_BY_ID);
            statement.setString(1, id);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит клиент найден
            if (resultSet.next()) {
                client = new Client();
                client.setId(resultSet.getInt("id"));
                client.setName(resultSet.getString("name"));
                client.setSurname(resultSet.getString("surname"));
                client.setPassport(resultSet.getString("passport"));
                client.setAddress(resultSet.getString("adress"));
            }
            LOGGER.trace("Client was founded");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection for client by id failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement for client by id failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return client;
    }

    public HashSet<BigDecimal> takeNoLinkedCardNumber() throws DAOException {
        LOGGER.info("Take no bind card numbers by admin");
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        HashSet<BigDecimal> freeCardNumbers = new HashSet<>();

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(NO_BIND_CARD_LIST);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит проверяем роль
            while (resultSet.next()) {
                freeCardNumbers.add(resultSet.getBigDecimal("cardNumber"));

                LOGGER.trace("Card number added");
            }
            LOGGER.trace("No bind card number set was formed");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection for No bind card number set failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement for No bind card number set failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return freeCardNumbers;
    }

    public void linkNewCard(String cardNumber, int clientId) throws DAOException {
        LOGGER.info("Link cardNumber: " + cardNumber + " to client id: " + clientId);
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CARD_LINK);
            statement.setInt(1, clientId);
            statement.setString(2, cardNumber);

            LOGGER.debug("Prepare Statement finished");

            statement.executeUpdate();
            LOGGER.trace("Execute statement finished");
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Link new card failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Link new card statement failed.", e);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public void deleteCard(String cardNumber) throws DAOException {

        LOGGER.info("Delete card by number: " + cardNumber);
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            try {
                connection = ConnectionPool.getInstance().takeConnection();
                connection.setAutoCommit(false);                    //for transaction

                statement = connection.prepareStatement(DEL_CARD_HOLDER);
                statement.setString(1, cardNumber);
                statement.executeUpdate();

                statement = connection.prepareStatement(DEL_CARD);
                statement.setString(1, cardNumber);
                statement.executeUpdate();

                connection.commit();                            //for transaction
                connection.setAutoCommit(true);

            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException("Delete card by number prepare statement failed.", e);
            }
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Delete card by number failed.", exp);
        } catch (SQLException exp) {
            throw new DAOException("Transaction delete card by number failed", exp);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public boolean checkClient(String clientId) throws DAOException {
        LOGGER.info("Check client by id from table 'userinfo': " + clientId);
        ProxyConnection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet;
        boolean isClientExist = false;

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(CHECK_ID);
            statement.setString(1, clientId);

            LOGGER.debug("Prepare Statement finished");

            resultSet = statement.executeQuery();

            //если в resulset есть хоть одна запись, значит клиент найден
            if (resultSet.next()) {
                isClientExist = true;
                LOGGER.trace("Client was founded");
            }

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection to check client by id failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement to check client by id failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

        return isClientExist;
    }

    public void deleteClient(int clientRealId) throws DAOException {
        LOGGER.info("Delete client by user.id: " + clientRealId);
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        //подготовка запроса
        try {
            connection = ConnectionPool.getInstance().takeConnection();
            statement = connection.prepareStatement(DELETE_CLIENT);
            statement.setInt(1, clientRealId);

            LOGGER.debug("Prepare Statement finished");

            statement.executeUpdate();

        } catch (ConnectionPoolException exp) {
            throw new DAOException("Connection delete client by user.id failed.", exp);
        } catch (SQLException e) {
            throw new DAOException("Prepare statement delete client by user.id failed.", e);
        } finally {
            close(statement);
            //return Connection to the ConnectionPool instead of closing it
            connection.close();
        }

    }

    public void updateClientData(int infoId, String name, String surname, String passport, String address,String login,
                             String password) throws DAOException {

        LOGGER.info("Update client data");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            try {
                connection = ConnectionPool.getInstance().takeConnection();
                connection.setAutoCommit(false);                    //for transaction

                statement = connection.prepareStatement(UPDATE_CLIENT);
                LOGGER.trace("Update login: " + login +", pass: " + password);
                statement.setString(1, login);
                statement.setString(2, password);
                statement.setInt(3, infoId);
                statement.executeUpdate();

                LOGGER.trace("Update name:" + name + ":" + surname + ":" + passport + ":" + address);
                statement = connection.prepareStatement(UPDATE_CLIENT_INFO);
                statement.setString(1, name);
                statement.setString(2, surname);
                statement.setString(3, passport);
                statement.setString(4, address);
                statement.setInt(5, infoId);
                statement.executeUpdate();

                connection.commit();                            //for transaction
                connection.setAutoCommit(true);

            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException("Update client data prepare statement failed.", e);
            }
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Update client data failed.", exp);
        } catch (SQLException exp) {
            throw new DAOException("Transaction update client data failed", exp);
        } finally {
            close(statement);
            connection.close();
        }

    }

    public void updateCardData(String valid, String csc, String cardType, String currency,
                               String amount, String status, int bankAccId) throws DAOException {

        LOGGER.info("Update card data");
        ProxyConnection connection = null;
        PreparedStatement statement = null;

        try {
            try {
                connection = ConnectionPool.getInstance().takeConnection();
                connection.setAutoCommit(false);                    //for transaction

                statement = connection.prepareStatement(UPDATE_BANK_ACC);
                LOGGER.trace("Update bank account where id: " + bankAccId);
                statement.setString(1, currency);
                statement.setString(2, amount);
                statement.setInt(3, bankAccId);
                statement.executeUpdate();
                LOGGER.trace("Bank account updated.");

                statement = connection.prepareStatement(UPDATE_CARD);
                statement.setString(1, valid);
                statement.setString(2, csc);
                statement.setString(3, cardType);
                statement.setString(4, status);
                statement.setInt(5, bankAccId);
                statement.executeUpdate();
                LOGGER.trace("Card updated.");

                connection.commit();                            //for transaction
                connection.setAutoCommit(true);

            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DAOException("Update client data prepare statement failed.", e);
            }
        } catch (ConnectionPoolException exp) {
            throw new DAOException("Update client data failed.", exp);
        } catch (SQLException exp) {
            throw new DAOException("Transaction update client data failed", exp);
        } finally {
            close(statement);
            connection.close();
        }

    }

    private void close(PreparedStatement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Failed prepare statement closing");
        }
    }
}
