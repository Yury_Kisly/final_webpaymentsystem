package by.epam.paysys.constant;


public final class ProjectConst {
    public static final String CONFIG = "property.config";
    public static final String DB_MY_SQL = "property.dbMySQL";
    public static final String LOC = "property.loc";
    public static final String MESSAGE = "property.message";
}
