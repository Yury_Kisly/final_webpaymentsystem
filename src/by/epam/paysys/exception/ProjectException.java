package by.epam.paysys.exception;

import javax.servlet.ServletException;


public class ProjectException extends ServletException {
    public ProjectException(String str) { super(str); }
    public ProjectException(Exception e) { super(e); }
    public ProjectException(String str, Exception e) { super(str, e); }
}
