package by.epam.paysys.service;


import by.epam.paysys.dao.DAOException;
import by.epam.paysys.dao.ProjDAOFactory;
import by.epam.paysys.entity.Card;

import java.util.ArrayList;

public class CommonService implements ICommonService {

    @Override
    public int takeAccIdByNumber(String cardNumber) throws ServiceException {
        int accountId;
        try {
            accountId = ProjDAOFactory.takeCommonDAO().takeAccIdByNumber(cardNumber);
        } catch (DAOException e) {
            throw new ServiceException("Take bank account by card number dao failed.", e);
        }
        return accountId;
    }

    @Override
    public Card takeCardByNumber(String cardNumber) throws ServiceException {
        Card card;
        try {
            card = ProjDAOFactory.takeCommonDAO().takeCardByNumber(cardNumber);
        } catch (DAOException e) {
            throw new ServiceException("Take card by number dao failed.", e);
        }
        return card;
    }

    @Override
    public ArrayList<Card> takeCards(int clientId) throws ServiceException {
        ArrayList<Card> cards;
        try {
            cards = ProjDAOFactory.takeCommonDAO().takeCards(clientId);
        } catch (DAOException e) {
            throw new ServiceException("Take cards dao failed.", e);
        }

        return cards;
    }

    @Override
    public int takeTableUserId(int id) throws ServiceException {
        int realId;
        try {
            realId = ProjDAOFactory.takeCommonDAO().takeTableUserId(id);
        } catch (DAOException e) {
            throw new ServiceException("Delete card by card number dao failed.", e);
        }
        return realId;
    }
}
