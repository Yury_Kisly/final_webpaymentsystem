package by.epam.paysys.service;


import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

public interface IAdminService {

    ArrayList<Client> takeClients() throws ServiceException;
    ArrayList<Card> takeCards() throws ServiceException;
    void unblockCard(String cardNumber) throws ServiceException;
    void regNewClient(String name, String surname, String passport, String address,
                                                            String login, String password) throws ServiceException;

    boolean checkCard(String cardNumber) throws ServiceException;

    void regNewCard(String cardNumber, String valid, String csc, String cardType,
                                                            String currency, String amount) throws ServiceException;

    Client takeClientById(String id) throws ServiceException;
    HashSet<BigDecimal> takeNoLinkedCardNumber() throws ServiceException;
    void linkNewCard(String cardNumber, int clientId) throws ServiceException;
    void deleteCard(String cardNumber) throws ServiceException;
    boolean checkClient(String clientId) throws ServiceException;
    void deleteClient(int clientRealId) throws ServiceException;

    void updateClientData(int infoId, String name, String surname, String passport, String address,
                      String login, String password) throws ServiceException;

    void updateCardData(String valid, String csc, String cardType, String currency,
                        String amount, String status, int bankAccId) throws ServiceException;



}
