package by.epam.paysys.service;

public class ServiceException extends Exception {
    public ServiceException(String str) { super(str); }
    public ServiceException(Exception e) { super(e); }
    public ServiceException(String str, Exception e) { super(str, e); }
}
