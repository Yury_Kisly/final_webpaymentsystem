package by.epam.paysys.service;

import by.epam.paysys.dao.DAOException;
import by.epam.paysys.dao.ProjDAOFactory;
import by.epam.paysys.entity.Client;
import by.epam.paysys.entity.UserType;
import by.epam.paysys.md5.PasswordMD5;


public class LoginService implements ILoginService{


    @Override
    public UserType checkLogin(String login, String password) throws ServiceException {
        UserType userType;
        //После того как поля логина и пароля были проверены валидатором, делается запрос в Базу через DAO

        String hidePassword = new PasswordMD5().makeHash(password);                                    //password   MD5
        try {
            userType = ProjDAOFactory.takeUserDAO().checkUser(login, hidePassword);
        } catch (DAOException e) {
            throw new ServiceException("Check login dao failed.", e);
        }

        return userType;
    }

    @Override
    public Client takeClient(String login, String password) throws ServiceException {
        Client client;
        String hidePassword = new PasswordMD5().makeHash(password);                                    //password   MD5
        try {
            client = ProjDAOFactory.takeUserDAO().takeClient(login, hidePassword);
        } catch (DAOException e) {
            throw new ServiceException("Take client dao failed.", e);
        }

        return client;
    }
}
