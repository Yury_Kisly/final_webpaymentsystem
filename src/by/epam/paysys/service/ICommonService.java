package by.epam.paysys.service;


import by.epam.paysys.entity.Card;

import java.util.ArrayList;

public interface ICommonService {
    int takeAccIdByNumber(String cardNumber) throws ServiceException;
    Card takeCardByNumber(String cardNumber) throws ServiceException;
    ArrayList<Card> takeCards(int clientId) throws ServiceException;
    int takeTableUserId(int id) throws ServiceException;
}
