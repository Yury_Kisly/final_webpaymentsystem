package by.epam.paysys.service;


import by.epam.paysys.entity.Client;
import by.epam.paysys.entity.UserType;

public interface ILoginService {
    UserType checkLogin(String login, String password) throws ServiceException;
    Client takeClient(String login, String password) throws ServiceException;
}
