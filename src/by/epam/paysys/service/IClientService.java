package by.epam.paysys.service;

import by.epam.paysys.entity.Card;

import java.math.BigDecimal;


public interface IClientService {

    void blockCard(String cardNumber) throws ServiceException;
    void updateBankAccaunt(int accountId, BigDecimal newAmount) throws ServiceException;
    void makeTransfer(Card fromCard, Card toCard, int fromAccountId,
                                                int toAccountId, BigDecimal amount) throws ServiceException;
}
