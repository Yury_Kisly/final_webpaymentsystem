package by.epam.paysys.service;

import by.epam.paysys.dao.DAOException;
import by.epam.paysys.dao.ProjDAOFactory;
import by.epam.paysys.entity.Card;

import java.math.BigDecimal;


public class ClientService extends CommonService implements IClientService{

    @Override
    public void blockCard(String cardNumber) throws ServiceException {

        try {
            ProjDAOFactory.takeClientDAO().blockCard(cardNumber);
        } catch (DAOException e) {
            throw new ServiceException("Blocking client card dao failed.", e);
        }
    }

    @Override
    public void updateBankAccaunt(int accountId, BigDecimal newAmount) throws ServiceException {
        try {
            ProjDAOFactory.takeClientDAO().updateBankAccaount(accountId, newAmount);
        } catch (DAOException e) {
            throw new ServiceException("Update after payment dao failed.", e);
        }
    }

    @Override
    public void makeTransfer(Card fromCard, Card toCard, int fromAccountId,
                             int toAccountId, BigDecimal amount) throws ServiceException {

        //calculate what amount should be write to db
        BigDecimal fromAmount = fromCard.getSumm().subtract(amount);
        BigDecimal toAmount = toCard.getSumm().add(amount);

        try {
            ProjDAOFactory.takeClientDAO().makeTransfer(fromAccountId, toAccountId, fromAmount, toAmount);
        } catch (DAOException e) {
            throw new ServiceException("Card transfer dao failed.", e);
        }
    }


}
