package by.epam.paysys.service;


import by.epam.paysys.dao.DAOException;
import by.epam.paysys.dao.ProjDAOFactory;
import by.epam.paysys.entity.Card;
import by.epam.paysys.entity.Client;
import by.epam.paysys.md5.PasswordMD5;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

public class AdminService extends CommonService implements IAdminService {

    @Override
    public ArrayList<Client> takeClients() throws ServiceException {
        ArrayList<Client> clients = null;
        try {
            clients = ProjDAOFactory.takeAdminDAO().takeClients();
        } catch (DAOException e) {
            throw new ServiceException("Take clients list dao failed.", e);
        }
        return clients;
    }

    @Override
    public ArrayList<Card> takeCards() throws ServiceException {
        ArrayList<Card> cards = null;
        try {
            cards = ProjDAOFactory.takeAdminDAO().takeCards();
        } catch (DAOException e) {
            throw new ServiceException("Take cards list dao failed.", e);
        }
        return cards;
    }

    @Override
    public void unblockCard(String cardNumber) throws ServiceException {

        try {
            ProjDAOFactory.takeAdminDAO().unblockCard(cardNumber);
        } catch (DAOException e) {
            throw new ServiceException("Ublocking card dao failed.", e);
        }
    }

    @Override
    public void regNewClient(String name, String surname, String passport, String address,
                             String login, String password) throws ServiceException {

        String hidePassword = new PasswordMD5().makeHash(password);                          //password   MD5

        try {
            ProjDAOFactory.takeAdminDAO().regNewClient(name, surname, passport, address, login, hidePassword);
        } catch (DAOException e) {
            throw new ServiceException("New client registration info dao failed.", e);
        }
    }

    @Override
    public boolean checkCard(String cardNumber) throws ServiceException {
        boolean isCardExist = false;
        try {
            isCardExist = ProjDAOFactory.takeAdminDAO().checkCard(cardNumber);
        } catch (DAOException e) {
            throw new ServiceException("Check card by number dao failed.", e);
        }
        return isCardExist;
    }

    @Override
    public void regNewCard(String cardNumber, String valid, String csc, String cardType,
                                                            String currency, String amount) throws ServiceException {

        try {
            ProjDAOFactory.takeAdminDAO().regNewCard(cardNumber, valid, csc, cardType, currency, amount);
        } catch (DAOException e) {
            throw new ServiceException("New card registration dao failed.", e);
        }
    }

    @Override
    public Client takeClientById(String id) throws ServiceException {
        Client client;
        try {
            client = ProjDAOFactory.takeAdminDAO().takeClientById(id);
        } catch (DAOException e) {
            throw new ServiceException("Take client by id dao failed.", e);
        }
        return client;
    }

    @Override
    public HashSet<BigDecimal> takeNoLinkedCardNumber() throws ServiceException {
        HashSet<BigDecimal> freeCardNumbers;
        try {
            freeCardNumbers = ProjDAOFactory.takeAdminDAO().takeNoLinkedCardNumber();
        } catch (DAOException e) {
            throw new ServiceException("Take free card numbers dao failed.", e);
        }
        return freeCardNumbers;
    }

    @Override
    public void linkNewCard(String cardNumber, int clientId) throws ServiceException {
        try {
            ProjDAOFactory.takeAdminDAO().linkNewCard(cardNumber, clientId);
        } catch (DAOException e) {
            throw new ServiceException("Link new card dao failed.", e);
        }
    }

    @Override
    public void deleteCard(String cardNumber) throws ServiceException {
        try {
            ProjDAOFactory.takeAdminDAO().deleteCard(cardNumber);
        } catch (DAOException e) {
            throw new ServiceException("Delete card by card number dao failed.", e);
        }
    }

    @Override
    public boolean checkClient(String clientId) throws ServiceException {
        boolean isClientExist;
        try {
            isClientExist = ProjDAOFactory.takeAdminDAO().checkClient(clientId);
        } catch (DAOException e) {
            throw new ServiceException("Check client by id dao failed.", e);
        }
        return isClientExist;
    }

    @Override
    public void deleteClient(int clientRealId) throws ServiceException {
        try {
            ProjDAOFactory.takeAdminDAO().deleteClient(clientRealId);
        } catch (DAOException e) {
            throw new ServiceException("Delete client by user.id dao failed.", e);
        }
    }

    @Override
    public void updateClientData(int infoId, String name, String surname, String passport, String address,
                             String login, String password) throws ServiceException {

        String hidePassword = new PasswordMD5().makeHash(password);                          //password   MD5

        try {
            ProjDAOFactory.takeAdminDAO().updateClientData(infoId, name, surname, passport, address,
                    login, hidePassword);
        } catch (DAOException e) {
            throw new ServiceException("Update client data dao failed.", e);
        }
    }

    @Override
    public void updateCardData(String valid, String csc, String cardType, String currency,
                                    String amount, String status, int bankAccId) throws ServiceException {
        try {
            ProjDAOFactory.takeAdminDAO().updateCardData(valid, csc, cardType, currency,
                    amount, status, bankAccId);
        } catch (DAOException e) {
            throw new ServiceException("Update card data dao failed.", e);
        }
    }


}
